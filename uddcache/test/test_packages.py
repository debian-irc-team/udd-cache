# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for packages """

import pytest

from uddcache.udd import Udd
from uddcache.packages import Release, Package, SourcePackage, PackageNotFoundError
from uddcache.data import DebianData


stable = DebianData.stable_release
stablebpo = f"{DebianData.stable_release}-backports"


@pytest.mark.usefixtures("udd")
class TesteRelease:
    udd: Udd

    def testRelease(self) -> None:
        """Test binding the release"""
        rd = Release(self.udd)
        assert rd
        rs = Release(self.udd, release="sid")
        assert rs
        ra = Release(self.udd, arch="armhf")
        assert ra
        rb = Release(self.udd, release=[stable, stablebpo])
        assert rb

    def testPackage(self) -> None:
        """Test looking up an individual binary package"""
        # default release (implicitly stable/amd64)
        rd = Release(self.udd)
        assert rd.Package("test-package-all-releases")
        assert rd.Package("test-package-all-releases").Found()
        assert not rd.Package(
            "test-package-sid-only"
        ).Found(), "Check a package that is not in stable"

        # explicit sid release (implicitly amd64)
        rs = Release(self.udd, release="sid")
        assert rs.Package("test-package-sid-only").Found()
        assert not rs.Package("test-package-stable-only").Found()

        # explicit arch (implicitly stable)
        ra = Release(self.udd, arch="armhf", release="sid")
        assert not ra.Package(
            "test-package-sid-amd64-only"
        ).Found(), "Check a package that is not in this arch"
        assert ra.Package(
            "test-package-sid-armhf-only"
        ).Found(), "Check a package that is only in this arch"

    def testPackageReleaseOverlays(self) -> None:
        """Test looking up an individual binary package"""
        # overlay archive (backports)
        rb = Release(self.udd, release=[stable, stablebpo])
        p = rb.Package("test-package-all-releases")
        assert p.Found()
        assert p.data is not None
        assert "bpo" in p.data["version"]
        p = rb.Package("test-package-stable-only")
        assert p.Found()
        assert p.data is not None
        assert "bpo" not in p.data["version"]
        p = rb.Package("test-package-stable-backports-only")
        assert p.Found()
        assert p.data is not None
        assert "bpo" in p.data["version"]

        rb = Release(
            self.udd, release=[stable, stablebpo], pins="illegal value"  # type: ignore
        )
        with pytest.raises(ValueError):
            rb.Package("test-package-all-releases")

        rb = Release(self.udd, release=[stable, stablebpo])
        p = rb.Package("test-package-all-releases")
        assert p.data is not None
        assert p.data["version"] > "1.2.3"
        rb = Release(
            self.udd,
            release=[stable, stablebpo],
            pins={stable: 2, stablebpo: 1},
        )
        p = rb.Package("test-package-all-releases")
        assert p.data is not None
        assert p.data["version"] == "1.2.2-1", "Check pinning of package from stable"
        rb = Release(
            self.udd,
            release=[stable, stablebpo],
            pins={"no-such-release": 2},
        )
        p = rb.Package("test-package-all-releases")
        assert p.data is not None
        assert p.data["version"] > "1.2.3", "Nonsense pinning ignored"

    def testPackageVersionRestriction(self) -> None:
        # test
        rd = Release(self.udd)
        assert rd.Package(
            "test-package-all-releases", version="1.2.2", operator=">>"
        ).Found()
        assert not rd.Package(
            "test-package-all-releases", version="1.2.2", operator="<<"
        ).Found()

    def testSource(self) -> None:
        """Test looking up an individual source package"""
        rd = Release(self.udd)
        assert rd.Source("test-package-all-releases")
        assert rd.Source("test-package-all-releases").Found()
        with pytest.raises(PackageNotFoundError):
            rd.Source("nosuchpackage")
        assert rd.Source("test-package-different-name").Found()
        with pytest.raises(PackageNotFoundError):
            rd.Source(
                "test-package-different-name",
                autoBin2Src=False,
            )

    def testArchApplies(self) -> None:
        """Test matching arch names and wildcard archs"""
        rd = Release(self.udd, release="sid", arch="i386")
        assert rd.arch_applies("i386")
        assert not rd.arch_applies("amd64")
        assert rd.arch_applies("linux-any")
        assert rd.arch_applies("any-i386")
        assert not rd.arch_applies("any-amd64")
        assert not rd.arch_applies("kfreebsd-any")

        rd = Release(self.udd, release="sid", arch="kfreebsd-i386")
        assert rd.arch_applies("kfreebsd-i386")
        assert not rd.arch_applies("i386")
        assert not rd.arch_applies("linux-any")
        assert rd.arch_applies("any-i386")
        assert not rd.arch_applies("any-amd64")
        assert rd.arch_applies("kfreebsd-any")

    def testStrConversion(self) -> None:
        rd = Release(self.udd, release="sid", arch="amd64")
        assert str(rd)
        assert "Release" in str(rd)
        assert "sid" in str(rd)


@pytest.mark.usefixtures("udd")
class TestPackage:
    udd: Udd

    def testPackage(self) -> None:
        """Test binding a package"""
        with pytest.raises(ValueError):
            Package(self.udd)

        p = Package(self.udd, package="test-package-all-releases")
        assert p
        assert p.Found()

        p = Package(
            self.udd,
            package="test-package-sid-amd64-only",
            arch="armhf",
            release="sid",
        )
        assert p, "Test package that doesn't exist on specified arch"
        assert not p.Found(), "Test package that doesn't exist on specified arch"

        p = Package(self.udd, package="test-package-sid-only", release="sid")
        assert p
        assert p.Found()

        p = Package(self.udd, package="test-package-stable-only", release="sid")
        assert p
        assert not p.Found()

        p = Package(
            self.udd,
            package="test-package-stable-backports-only",
            release=[stable, stablebpo],
        )
        assert p
        assert p.Found()
        assert p.data is not None
        assert "bpo" in p.data["version"]

        p = Package(
            self.udd,
            package="test-package-stable-backports-only",
            release=stable,
        )
        assert p
        assert not p.Found()

        p = Package(
            self.udd,
            package="test-package-all-releases",
            release=[stable, stablebpo],
        )
        assert p
        assert p.Found()
        assert p.data is not None
        assert "bpo" in p.data["version"]

        p = Package(self.udd, package="nosuchpackage")
        assert not p.Found()

        p = Package(self.udd, package="nosuchpackage")
        assert not p.Found()
        p2 = Package(self.udd, package=p)
        assert not p2.Found()

        # check that it objects to the wrong types
        with pytest.raises(ValueError):
            Package(self.udd, package=list("abc"))  # type: ignore[arg-type]

    def testProviders(self) -> None:
        """Test searching for provided packages"""
        p = Package(self.udd, package="mail-transport-agent")
        assert len(p.ProvidersList()) >= 1, "Test rprovides for mail-transport-agent"

        p = Package(self.udd, package="nosuchpackage")
        assert len(p.ProvidersList()) == 0, "Test rprovides for non-existent package"

        p = Package(self.udd, package="test-provides-real-package")
        assert len(p.ProvidersList()), "Test rprovides for real+virtual package"

        p = Package(self.udd, package="test-provides-versioned-package")
        # check for no specified version
        providers = p.ProvidersList()
        assert len(providers) == 1, "Test rprovides for versioned provides"
        assert providers[0][1] == "1", "Test rprovides version for versioned provides"

        # check for specified version
        providers = p.ProvidersList("1")
        assert len(providers) == 1, "Test rprovides for versioned provides"
        assert providers[0][1] == "1", "Test rprovides version for versioned provides"

        # check for specified version that doesn't exist
        providers = p.ProvidersList("42")
        assert len(providers) == 0, "Test rprovides for versioned provides"

        p = Package(self.udd, package="test-provides-versioned-different-package")
        # check for no specified version
        providers = p.ProvidersList()
        assert len(providers) == 1, "Test rprovides for versioned provides"
        assert providers[0][1] == "3", "Test rprovides version for versioned provides"

        # check for specified version
        providers = p.ProvidersList("3")
        assert len(providers) == 1, "Test rprovides for versioned provides"
        assert providers[0][1] == "3", "Test rprovides version for versioned provides"

        # check for specified version that doesn't exist
        providers = p.ProvidersList("42")
        assert len(providers) == 0, "Test rprovides for versioned provides"

    def testIsVirtual(self) -> None:
        """Test virtual package identification"""
        # pure virtual, many providers
        p = Package(self.udd, package="mail-transport-agent")
        assert p.IsVirtual()

        # neither concrete nor virtual
        p = Package(self.udd, package="no-such-package")
        assert not p.IsVirtual()

        # concrete only
        p = Package(self.udd, package="test-package-all-releases")
        assert not p.IsVirtual()

        # both concrete and virtual
        p = Package(self.udd, package="test-provides-real-package")
        assert p.IsVirtual()

    def testIsVirtualOnly(self) -> None:
        """Test purely virtual package identification"""
        # pure virtual, many providers
        p = Package(self.udd, package="mail-transport-agent")
        assert p.IsVirtualOnly()

        # neither concrete nor virtual
        p = Package(self.udd, package="no-such-package")
        assert not p.IsVirtualOnly()

        # concrete only
        p = Package(self.udd, package="test-package-all-releases")
        assert not p.IsVirtualOnly()

        # both concrete and virtual
        p = Package(self.udd, package="test-provides-real-package")
        assert not p.IsVirtualOnly()

    def testIsAvailable(self) -> None:
        """Test package finding for concrete/virtual packges"""
        # pure virtual, many providers
        p = Package(self.udd, package="mail-transport-agent")
        assert p.IsAvailable()

        # neither concrete nor virtual
        p = Package(self.udd, package="no-such-package")
        assert not p.IsVirtual()

        # concrete only
        p = Package(self.udd, package="test-package-all-releases")
        assert p.IsAvailable()

        # both concrete and virtual
        p = Package(self.udd, package="test-provides-real-package")
        assert p.IsAvailable()

    def testRelationEntry(self) -> None:
        """Test getting the relationships between packages"""
        p = Package(self.udd, package="test-no-depends")
        assert not p.RelationEntry("pre_depends"), "Test package with no dependencies"
        assert not p.PreDepends(), "Test package with no dependencies"
        assert not p.RelationEntry("depends"), "Test package with dependencies"
        assert not p.Depends(), "Test package with dependencies"
        assert not p.RelationEntry("suggests"), "Test package with dependencies"
        assert not p.Suggests(), "Test package with dependencies"
        assert not p.RelationEntry("conflicts"), "Test package with dependencies"
        assert not p.Conflicts(), "Test package with dependencies"
        assert not p.RelationEntry("breaks"), "Test package with dependencies"
        assert not p.Breaks(), "Test package with dependencies"
        assert not p.RelationEntry("enhances"), "Test package with no dependencies"
        assert not p.Enhances(), "Test package with no dependencies"

        p = Package(self.udd, package="test-recommends")
        assert p.RelationEntry("recommends"), "Test package with dependencies"
        assert p.Recommends(), "Test package with dependencies"

        p = Package(self.udd, package="test-pre-depends")
        # Depends will include Pre-Depends
        assert p.RelationEntry("depends"), "Test package with dependencies"
        assert p.Depends(), "Test package with dependencies"
        assert p.RelationEntry("pre_depends"), "Test package with dependencies"
        assert p.PreDepends(), "Test package with dependencies"
        assert p.RelationEntry("breaks"), "Test package with dependencies"
        assert p.Breaks(), "Test package with dependencies"
        assert p.RelationEntry("replaces"), "Test package with dependencies"
        assert p.Replaces(), "Test package with dependencies"

        p = Package(self.udd, package="nosuchpackage")
        with pytest.raises(LookupError):
            p.RelationEntry("depends")

        p = Package(self.udd, package="test-pre-depends")
        with pytest.raises(KeyError):
            p.RelationEntry("nosuchrelation")

    def testRelationEntryList(self) -> None:
        """Test getting the relationships between packages as a structure"""
        p = Package(self.udd, package="test-depends")
        assert (
            len(p.RelationshipOptionsList("pre_depends")) == 0
        ), "Test package with dependencies"
        assert len(p.PreDependsList()) == 0, "Test package with dependencies"
        assert (
            len(p.RelationshipOptionsList("depends")) == 2
        ), "Test package with dependencies"
        assert len(p.DependsList()) == 2, "Test package with dependencies"
        assert (
            len(p.RelationshipOptionsList("recommends")) == 0
        ), "Test package with dependencies"
        assert len(p.RecommendsList()) == 0, "Test package with dependencies"
        assert (
            len(p.RelationshipOptionsList("suggests")) == 0
        ), "Test package with dependencies"
        assert len(p.SuggestsList()) == 0, "Test package with dependencies"
        assert (
            len(p.RelationshipOptionsList("conflicts")) == 0
        ), "Test package with dependencies"
        assert len(p.ConflictsList()) == 0, "Test package with dependencies"
        assert (
            len(p.RelationshipOptionsList("enhances")) == 0
        ), "Test package with no dependencies"
        assert len(p.EnhancesList()) == 0, "Test package with no dependencies"
        assert (
            len(p.RelationshipOptionsList("breaks")) == 0
        ), "Test package with no dependencies"
        assert len(p.BreaksList()) == 0, "Test package with no dependencies"
        assert (
            len(p.RelationshipOptionsList("replaces")) == 0
        ), "Test package with no dependencies"
        assert len(p.ReplacesList()) == 0, "Test package with no dependencies"

        p = Package(self.udd, package="test-depends-options")
        assert (
            len(p.RelationshipOptionsList("depends")) == 2
        ), "Test package with dependencies"
        assert len(p.DependsList()) == 2, "Test package with dependencies"

        p = Package(self.udd, package="test-pre-depends")
        # pre-depends appear in depends too
        assert (
            len(p.RelationshipOptionsList("depends")) == 1
        ), "Test package with dependencies"
        assert len(p.DependsList()) == 1, "Test package with dependencies"

        p = Package(self.udd, package="nosuchpackage")
        with pytest.raises(LookupError):
            p.RelationshipOptionsList("depends")

        p = Package(self.udd, package="test-depends")
        with pytest.raises(KeyError):
            p.RelationshipOptionsList("nosuchrelation")

    def testStrConversion(self) -> None:
        p = Package(self.udd, package="test-package-all-releases")
        assert str(p)
        assert "test-package-all-releases" in str(p)

        p = Package(self.udd, package="no-such-package")
        assert str(p)
        assert "no-such-package" in str(p)


@pytest.mark.usefixtures("udd")
class TestSourcePackage:
    udd: Udd

    def testPackage(self) -> None:
        """Test binding a package"""
        with pytest.raises(ValueError):
            Package(self.udd)

        p = SourcePackage(self.udd, package="test-package-all-releases")
        assert p
        assert p.Found()

        p = SourcePackage(self.udd, package="test-source-package-different-name")
        assert p
        assert p.Found()

        # auto bin2src mapping not done when only looking at source packages
        p = SourcePackage(self.udd, package="test-package-different-name")
        assert p
        assert not p.Found()

        p = SourcePackage(self.udd, package="nosuchpackage")
        assert not p.Found()

    def testBinaries(self) -> None:
        """Test listing the binary packages compiled by a source package"""
        p = SourcePackage(self.udd, package="test-package-all-releases")
        assert len(p.Binaries()) == 1

        p = SourcePackage(self.udd, package="test-source-package-multiple-binaries")
        assert len(p.Binaries()) == 3

        p = SourcePackage(self.udd, package="nosuchpackage")
        assert not p.Binaries()

    def testBuildDeps(self) -> None:
        """Test listing the build-dep and build-dep-indep packages"""
        ps = SourcePackage(self.udd, package="test-package-all-releases")
        assert ps.BuildDepends()
        assert not ps.BuildDependsIndep()
        assert ps.BuildDependsList()
        assert not ps.BuildDependsIndepList()

        ps = SourcePackage(self.udd, package="test-source-bdi")
        assert not ps.BuildDepends()
        assert ps.BuildDependsIndep()
        assert not ps.BuildDependsList()
        assert ps.BuildDependsIndepList()

        # package has no build-deps-indep
        ps = SourcePackage(self.udd, package="test-source-no-bd")
        assert not ps.BuildDependsIndep()
        assert not ps.BuildDependsIndepList()

    def testStrConversion(self) -> None:
        p = SourcePackage(self.udd, package="test-source-package-different-name")
        assert str(p)
        assert "test-source-package-different-name" in str(p)

        p = SourcePackage(self.udd, package="no-such-package")
        assert str(p)
        assert "no-such-package" in str(p)


class TestPackageNotFoundError:
    def testInit(self) -> None:
        assert PackageNotFoundError("packagename")

    def testStrConversion(self) -> None:
        e = PackageNotFoundError("packagename")
        assert "packagename" in str(e)
