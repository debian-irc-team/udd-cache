# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for bts UDD bindings """

from typing import Generator

import pytest

from uddcache.udd import Udd
from uddcache.bts import Bts, BugNotFoundError, Bugreport


class TestBts:
    @pytest.fixture
    def bts(self, udd: Udd) -> Generator[Bts, None, None]:
        yield udd.Bts(True)

    @pytest.fixture
    def btsc(self, udd: Udd) -> Generator[Bts, None, None]:
        yield udd.Bts(False)

    def test_bugs(self, bts: Bts, btsc: Bts) -> None:
        bugs = [500000, 600001, 500001, 500002, 600000]
        assert bts.bugs(bugs)
        assert len(bts.bugs(bugs)) == 5
        assert len(btsc.bugs(bugs)) == 2

    def test_bug(self, bts: Bts, btsc: Bts) -> None:
        b = bts.bug(500000)
        assert b
        assert b.title == "Please file more bugs"
        assert b.id == 500000
        assert b.severity == "wishlist"
        assert b.status == "done"
        assert b.archived is True
        assert bts.bug("500000")
        assert bts.bug("#500000")
        with pytest.raises(BugNotFoundError):
            bts.bug(-1)
        with pytest.raises(BugNotFoundError):
            bts.bug(99999999)
        with pytest.raises(BugNotFoundError):
            btsc.bug(500000)  # bug is archived

    def test_get_bugs(self, bts: Bts, btsc: Bts) -> None:
        assert bts.get_bugs({"package": "test-package-all-releases"})
        assert bts.get_bugs({"source": "test-package-all-releases"})
        # package -> source mapping
        assert bts.get_bugs({"package": "test-package-different-name"})
        assert bts.get_bugs({"source": "test-source-package-different-name"})
        assert btsc.get_bugs({"title": "Quite "})
        assert bts.get_bugs({"source": "test-only-archived-bugs"})
        assert not btsc.get_bugs({"source": "test-only-archived-bugs"})
        assert bts.get_bugs(
            {"source": "test-package-all-releases", "sort": "id", "sort_dir": "DESC"}
        )
        assert (
            len(bts.get_bugs({"source": "test-package-all-releases", "limit": 1})) == 1
        )

    def test_get_bugs_tags(self, bts: Bts) -> None:
        b = bts.bug(600000)
        bts.get_bugs_tags(b)
        assert len(b.tags) == 1
        b = bts.bug(500001)
        bts.get_bugs_tags(b)
        assert len(b.tags) == 4
        bl = bts.bugs([500000, 500001, 500002, 600001])
        bts.get_bugs_tags(bl)


class TestBugReport:
    def test_load(self) -> None:
        b = Bugreport()
        assert b.id is None
        assert b.title is None
        assert b.package is None
        data = {
            "id": 12345,
            "package": "foo",
            "title": "a really nasty bug",
            "affects_stable": True,
        }
        b = Bugreport(data)
        assert b.id == 12345
        assert b.package == "foo"
        assert b.affects_stable is True
        data = {
            "id": "12345",
            "package": "foo",
            "title": "a really nasty bug",
            "archived": 0,
        }
        b = Bugreport(data)
        assert b.id == 12345
        assert b.package == "foo"
        assert b.archived is False

    def test_readable_status(self) -> None:
        assert Bugreport({"status": "pending"}).readable_status == "open"
        assert Bugreport({"status": "pending-fixed"}).readable_status == "pending"
        assert Bugreport({"status": "fixed"}).readable_status == "fixed"
        assert Bugreport({"status": "done"}).readable_status == "closed"

    def test_wnpp_type(self) -> None:
        assert (
            Bugreport(
                {"package": "wnpp", "title": "ITA: somepackage -- the description"}
            ).wnpp_type
            == "ITA"
        )
        assert (
            Bugreport(
                {"package": "wnpp", "title": "RFP -- somepackage -- the description"}
            ).wnpp_type
            == "RFP"
        )
        assert (
            Bugreport(
                {"package": "wnpp", "title": "some other badly titled bug"}
            ).wnpp_type
            is None
        )
        with pytest.raises(ValueError):
            getattr(Bugreport(), "wnpp_type")

    def test_str_conversion(self) -> None:
        data = {"id": 12345, "package": "foo", "title": "a really nasty bug"}
        b = Bugreport(data)
        assert str(b)
        b.tags = ["help"]
        assert str(b)
        b.tags = ["help", "moreinfo"]
        assert str(b)


class TestBugNotFoundError:
    def test_str_conversion(self) -> None:
        e = BugNotFoundError(12345)
        assert "12345" in str(e)
        e = BugNotFoundError("12345")
        assert "12345" in str(e)
