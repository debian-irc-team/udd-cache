# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

"""Unit test for cli.py"""

# pylint: disable=too-many-lines

import shlex
from typing import Any, Generator, List, Optional
from unittest.mock import MagicMock

from debian.deb822 import Deb822
import pytest

from uddcache.packages_cli import Cli
from uddcache.udd import Udd, MaxRetriesExceeded
from uddcache.data import DebianData

from uddcache.test.test_clibase import TestCliBase, TestCliBaseParseArg
from uddcache.test.conftest import find_test_data_file


stable_release = DebianData.stable_release


@pytest.fixture(name="cli")
def fixture_cli(udd: Udd) -> Generator[Cli, None, None]:
    class dummyOptions:
        def __init__(self) -> None:
            self.distro = "debian"
            self.verbose = False

    cli = Cli(options=dummyOptions(), uddInstance=udd)
    yield cli


class TestPackagesCli(TestCliBase):
    # pylint: disable=too-many-public-methods

    def test_notfound(self, cli: Cli) -> None:
        with self.assertInStdOut(["no-such-package", "No package named"]):
            cli.notfound("no-such-package")

        with self.assertInStdOut(
            ["no-such-package", "No package named", "in myrelease"]
        ):
            cli.notfound("no-such-package", release="myrelease")

        with self.assertInStdOut(["no-such-package", "No package named", "in myarch"]):
            cli.notfound("no-such-package", arch="myarch")

        with self.assertInStdOut(
            ["no-such-package", "No package named", "in myrelease/myarch"]
        ):
            cli.notfound("no-such-package", release="myrelease", arch="myarch")

        with self.assertInStdOut(["no-such-package", "foo ", " bar "]):
            cli.notfound("no-such-package", message="foo %s bar %s")

        with pytest.raises(TypeError):
            cli.notfound(
                "no-such-package",
                arch="i386",
                release="stable",
                message="bad format %s",
            )

        with pytest.raises(TypeError):
            cli.notfound("no-such-package", message="bad format %s")

    def test_versions(self, cli: Cli) -> None:
        with self.assertInStdOut(
            ["test-package-all-releases", "1.2.3-1", "sid", "-backports"]
        ):
            assert cli.versions("test-package-all-releases")

        with self.assertInStdOut(["test-package-all-releases", "armhf"]):
            assert cli.versions("test-package-all-releases", arch="armhf")

        with self.assertInStdOut(["test-package-all-releases", "armhf"]):
            assert cli.versions("test-package-all-releases", args=["armhf"])

        with self.assertInStdOut(["test-package-all-releases", stable_release]):
            assert cli.versions("test-package-all-releases", stable_release)

        with self.assertInStdOut(["test-package-all-releases", stable_release]):
            assert cli.versions("test-package-all-releases", args=[stable_release])

        with self.assertInStdOut(
            ["test-package-all-releases", stable_release, "armhf"]
        ):
            assert cli.versions(
                "test-package-all-releases", args=[stable_release, "armhf"]
            )

        with self.assertInStdOut(["test-package-sid-only", "2.3.4-3", "sid"]):
            assert cli.versions("test-package-sid-only")

        with self.assertInStdOut(["test-package-sid-armhf-only", "No package named"]):
            assert not cli.versions("test-package-sid-armhf-only")

        with self.assertInStdOut(["test-package-sid-armhf-only", "3.4.5"]):
            assert cli.versions(
                "test-package-sid-armhf-only",
                arch="armhf",
            )

        with self.assertInStdOut(["test-package-sid-armhf-only", "3.4.5"]):
            assert cli.versions("test-package-sid-armhf-only", args=["armhf"])

        with self.assertInStdOut(["no-such-package", "No package named"]):
            assert not cli.versions("no-such-package")

    def test_versions_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.versions("no-such-package")

    def test_info(self, cli: Cli) -> None:
        with self.assertInStdOut(
            ["Package: test-package-all-releases", "Version: 1.2.2-1"]
        ):
            assert cli.info("test-package-all-releases")

        with self.assertInStdOut(["test-package-contrib", "1.2.3-1"]):
            assert cli.info("test-package-contrib", "sid")

        with self.assertInStdOut(["test-package-sid-armhf-only", "Version: 3.4.5"]):
            assert cli.info("test-package-sid-armhf-only", args=["armhf", "sid"])

        with self.assertInStdOut(["No package named", "no-such-package"]):
            assert not cli.info("no-such-package")

    def test_info_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.info("no-such-package")

    def test_names(self, cli: Cli) -> None:
        with self.assertInStdOut(["test-package-all-releases"]):
            assert cli.names("test-package-all-*")

        with self.assertInStdOut(
            ["test-package-stable-only"], ["test-package-sid-armhf-only"]
        ):
            assert cli.names("test-*-only")

        with self.assertInStdOut(
            ["test-package-sid-armhf-only"], ["test-package-stable-only"]
        ):
            assert cli.names("test-*-only", args=["armhf", "sid"])

        with self.assertInStdOut(
            ["No packages matching"],
            ["test-package-sid-armhf-only", "test-package-stable-only"],
        ):
            assert not cli.names("test-*-only", stable_release, "armhf")

        with self.assertInStdOut(["No packages matching"]):
            assert not cli.names("no-such-package")

    def test_names_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.names("no-such-package")

    def test_archs(self, cli: Cli) -> None:
        with self.assertInStdOut(["amd64"], ["armhf"]):
            assert cli.archs("test-package-sid-only", "sid")

        with self.assertInStdOut(["No package named"]):
            assert not cli.archs("test-package-sid-only")

        with self.assertInStdOut(["amd64", "armhf"]):
            assert cli.archs("test-package-all-releases", args=["sid"])

        with self.assertInStdOut(["armhf"], ["amd64"]):
            assert cli.archs("test-package-sid-armhf-only", "sid")

        with self.assertInStdOut(["No package named"]):
            assert not cli.archs("no-such-package")

    def test_archs_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.archs("no-such-package")

    def test_rprovides(self, cli: Cli) -> None:
        with self.assertInStdOut(["test-package-all-releases is a real package"]):
            assert cli.rprovides("test-package-all-releases")

        with self.assertInStdOut(
            ["is provided by", "test-provides-mta-1", "test-provides-mta-2"]
        ):
            assert cli.rprovides("mail-transport-agent")

        with self.assertInStdOut(
            ["is provided by", "test-provides-mta-1"], ["test-provides-mta-2"]
        ):
            assert cli.rprovides("mail-transport-agent", "sid")

        with self.assertInStdOut(
            ["test-provides-virtual-package", "is also a real package"]
        ):
            assert cli.rprovides("test-provides-real-package")

        with self.assertInStdOut(
            ["is provided by", "test-versioned-provides-real-package", "(= 1)"],
        ):
            assert cli.rprovides("test-provides-versioned-package", "stable")

        with self.assertInStdOut(
            ["is provided by", "test-versioned-provides-different-version", "(= 3)"],
        ):
            assert cli.rprovides("test-provides-versioned-different-package", "stable")

        with self.assertInStdOut(
            ["is provided by", "test-versioned-provides-different-version", "(= 3)"],
        ):
            assert cli.rprovides(
                "test-provides-versioned-different-package", "stable", version="3"
            )

        with self.assertInStdOut(
            ["No packages provide", "(= 1234)"],
        ):
            assert not cli.rprovides(
                "test-provides-versioned-different-package", "stable", version="1234"
            )

        with self.assertInStdOut(["No packages provide"]):
            assert not cli.rprovides("no-such-package")

    def test_rprovides_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.rprovides("no-such-package")

    def test_provides(self, cli: Cli) -> None:
        with self.assertInStdOut(["provides no additional packages"]):
            assert cli.provides("test-package-all-releases")

        with self.assertInStdOut(["mail-transport-agent"]):
            assert cli.provides("test-provides-mta-1")

        with self.assertInStdOut(["test-provides-versioned-package", "(= 1)"]):
            assert cli.provides("test-versioned-provides-real-package")

        with self.assertInStdOut(
            ["test-provides-versioned-different-package", "(= 3)"]
        ):
            assert cli.provides("test-versioned-provides-different-version")

        with self.assertInStdOut(["No package named"]):
            assert not cli.provides("test-provides-mta-1", arch="armhf")

        with self.assertInStdOut(["No package named"]):
            assert not cli.provides("test-provides-mta-1", args=["armhf"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.provides("test-provides-mta-2", release="sid")

        with self.assertInStdOut(["No package named"]):
            assert not cli.provides("test-provides-mta-2", args=["sid"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.provides("no-such-package")

    def test_provides_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.provides("no-such-package")

    def test_source(self, cli: Cli) -> None:
        with self.assertInStdOut(["Source: test-package-all-releases"]):
            assert cli.source("test-package-all-releases")

        with self.assertInStdOut(["test-source-package-different-name"]):
            assert cli.source("test-package-different-name")

        with self.assertInStdOut(["test-source-package-different-name"]):
            assert cli.source("test-package-different-name", release="sid")

        with self.assertInStdOut(["No package named"]):
            assert not cli.source("no-such-package")

    def test_source_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.source("no-such-package")

    def test_binaries(self, cli: Cli) -> None:
        with self.assertInStdOut(["Binaries: test-package-all-releases"]):
            assert cli.binaries("test-package-all-releases")

        with self.assertInStdOut(
            [
                "test-package-multibin-1",
                "test-package-multibin-2",
                "test-package-multibin-3",
            ]
        ):
            assert cli.binaries("test-source-package-multiple-binaries")

        with self.assertInStdOut(["test-package-different-name"]):
            assert cli.binaries("test-package-different-name")

        with self.assertInStdOut(["test-package-different-name"]):
            assert cli.binaries("test-source-package-different-name")

        with self.assertInStdOut(["No package named"]):
            assert not cli.binaries("no-such-package")

    def test_binaries_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.binaries("no-such-package")

    def test_builddeps(self, cli: Cli) -> None:
        with self.assertInStdOut(
            [
                "Package: test-package-all-releases",
                "Build-Depends: libtesta1, libtestb1",
                "Build-Depends-Indep: None",
            ]
        ):
            assert cli.builddeps("test-package-all-releases")

        with self.assertInStdOut(
            ["Build-Depends: None", "Build-Depends-Indep: libtesta1, libtestb1"]
        ):
            assert cli.builddeps("test-source-bdi")

        with self.assertInStdOut(["Build-Depends: None", "Build-Depends-Indep: None"]):
            assert cli.builddeps("test-source-no-bd")

        with self.assertInStdOut(["test-package-sid-armhf-only [armhf]"]):
            assert cli.builddeps("test-source-package-arch-specific-bd", "sid")

        with self.assertInStdOut(["Build-Depends"]):
            # builddeps doesn't try to install everything
            assert cli.builddeps(
                "test-source-package-arch-specific-bd-unsatisfiable", "sid"
            )

        with self.assertInStdOut(
            ["Build-Depends: test-package-all-releases, no-such-package"]
        ):
            assert cli.builddeps("test-source-package-bd-unsatifiable", args=["sid"])

        with self.assertInStdOut(["Build-Depends: test-provides-versioned-package"]):
            assert cli.builddeps(
                "test-source-package-bd-virtual-versioned", args=["sid"]
            )

        with self.assertInStdOut(["Build-Depends: test-provides-versioned-package"]):
            assert cli.builddeps(
                "test-source-package-bd-virtual-versioned-dep", args=["sid"]
            )

        with self.assertInStdOut(
            ["Build-Depends: test-provides-versioned-different-package"]
        ):
            assert cli.builddeps(
                "test-source-package-bd-virtual-versioned-dep-diff", args=["sid"]
            )

        with self.assertInStdOut(["Build-Depends"]):
            # builddeps doesn't try to install everything
            assert cli.builddeps(
                "test-source-package-bd-virtual-versioned-dep-unsatifiable", "sid"
            )

        with self.assertInStdOut(["No package named"]):
            assert not cli.builddeps("no-such-package")

    def test_builddeps_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.builddeps("no-such-package")

    def test_relations(self, cli: Cli) -> None:
        with self.assertInStdOut(
            [
                "test-package-all-releases",
                "Depends: libtesta1, libtestb1",
                "Recommends: None",
            ]
        ):
            assert cli.relations("test-package-all-releases")

        with self.assertInStdOut(["test-package-stable-only", "Breaks: None"]):
            assert cli.relations("test-package-stable-only")

        with self.assertInStdOut(["No package named"]):
            assert not cli.relations("test-package-stable-only", args=["sid"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.relations("test-package-stable-only", release="sid")

        with self.assertInStdOut(["No package named"]):
            assert not cli.relations("test-package-stable-only", arch="armhf")

        with self.assertInStdOut(["test-package-sid-armhf-only"]):
            assert cli.relations("test-package-sid-armhf-only", args=["armhf", "sid"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.relations("test-package-sid-armhf-only", args=["armhf"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.relations("test-package-sid-armhf-only", args=["sid"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.relations("no-such-package")

    def test_relations_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.relations("no-such-package")

    def test_depends(self, cli: Cli) -> None:
        with self.assertInStdOut(
            ["Package: test-package-all-releases", "Depends: libtesta1, libtestb1"]
        ):
            assert cli.depends("depends", "test-package-all-releases")

        with self.assertInStdOut(
            ["Package: test-package-all-releases", "Recommends: None"],
            ["Depends", "Suggests"],
        ):
            assert cli.depends("recommends", "test-package-all-releases")

        with self.assertInStdOut(["Recommends: libtesta1"]):
            assert cli.depends("recommends", "test-recommends")

        with self.assertInStdOut(["Suggests: None"]):
            assert cli.depends("suggests", "test-package-all-releases")

        with self.assertInStdOut(["No package named"]):
            assert not cli.depends("suggests", "test-suggests", release="sid")

        with self.assertInStdOut(["No package named"]):
            assert not cli.depends("suggests", "test-suggests", args=["sid"])

        with self.assertInStdOut(["Suggests: libtesta1"]):
            assert cli.depends("suggests", "test-suggests")

        with self.assertInStdOut(["Enhances: None"]):
            assert cli.depends("enhances", "test-package-all-releases")

        with self.assertInStdOut(["test-enhances", "Enhances: test-recommends"]):
            assert cli.depends("enhances", "test-enhances")

        with self.assertInStdOut(["Conflicts: None"]):
            assert cli.depends("conflicts", "test-package-all-releases")

        with self.assertInStdOut(["Conflicts: libtestb1"]):
            assert cli.depends("conflicts", "test-pre-depends")

        with self.assertInStdOut(["Breaks: libtestb1"]):
            assert cli.depends("breaks", "test-pre-depends")

        with self.assertInStdOut(["Replaces: libtestb1"]):
            assert cli.depends("replaces", "test-pre-depends")

        with self.assertInStdOut(["No package named"]):
            assert not cli.depends("depends", "no-such-package")

        # FIXME invalid relation not currently checked?
        # with self.assertRaises(ValueError):
        # cli.depends("no-such-relation", "no-such-package")

    def test_depends_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.depends("depends", "no-such-package")

    def test_recent(self, cli: Cli) -> None:
        with self.assertInStdOut(["J Random Hacker", "1.2.3-1"]):
            assert cli.recent("test-package-all-releases")

        with self.assertInStdOut(["2000-03-01", "J Random Hacker"]):
            assert cli.recent("test-package-different-name")

        with self.assertInStdOut(["No package named"]):
            assert not cli.recent("no-such-package")

    def test_recent_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.recent("no-such-package")

    def test_maint(self, cli: Cli) -> None:
        with self.assertInStdOut(["Changer: J Random Hacker"]):
            assert cli.maint("test-package-all-releases")

        with self.assertInStdOut(
            [
                "Version: 1.2.2-6.1",
                "Changer: Some Other Hacker <soh@example.com>",
                "NMU: yes",
            ]
        ):
            assert cli.maint("test-package-all-releases", "1.2.2-6.1")

        with self.assertInStdOut(["Version: 1.2.3-1"]):
            assert cli.maint("test-package-different-name")

        with self.assertInStdOut(["No package named"]):
            assert not cli.maint("no-such-package")

    def test_maint_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.maint("no-such-package")

    def test_popcon(self, cli: Cli) -> None:
        with self.assertInStdOut(
            ["Popcon data for test-package-all-releases", "installed: 42"]
        ):
            assert cli.popcon("test-package-all-releases")

        with self.assertInStdOut(["very-boring-package", "vote:"]):
            assert cli.popcon("very-boring-package")

        with self.assertInStdOut(["No package named"]):
            assert not cli.popcon("no-such-package")

    def test_popcon_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.popcon("no-such-package")

    def test_checkdeps(self, cli: Cli) -> None:
        with self.assertInStdOut(
            ["Depends: satisfied", "Recommends: satisfied", "Suggests: satisfied"]
        ):
            assert cli.checkdeps("test-package-all-releases")

        with self.assertInStdOut(["Recommends: satisfied"], ["Depends", "Suggests"]):
            assert cli.checkdeps("test-package-all-releases", deptypes=["recommends"])

        with self.assertInStdOut(["Suggests: satisfied"]):
            assert cli.checkdeps("test-suggests", deptypes=["suggests"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.checkdeps("test-package-sid-only")

        with self.assertInStdOut(["Depends: satisfied"]):
            assert cli.checkdeps("test-package-sid-only", args=["sid"])

        with self.assertInStdOut(["Depends: satisfied"]):
            assert cli.checkdeps("test-package-sid-only", release="sid")

        with self.assertInStdOut(["Depends: unsatisfied", "libtesta1"]):
            assert not cli.checkdeps(
                "test-package-sid-armhf-only", args=["armhf", "sid"]
            )

        with self.assertInStdOut(["No package named"]):
            assert not cli.checkdeps("no-such-package")

    def test_checkdeps_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.checkdeps("no-such-package")

    def test_checkbuilddeps(self, cli: Cli) -> None:
        with self.assertInStdOut(
            [
                "Build-dependency check for test-package-all-releases",
                "All build-dependencies satisfied",
            ]
        ):
            assert cli.checkbuilddeps("test-package-all-releases")

        with self.assertInStdOut(
            [
                "Build-dependency check for test-package-all-releases in sid/amd64",
                "sid: libtesta1, libtestb1",
            ]
        ):
            assert cli.checkbuilddeps("test-package-all-releases", release="sid")

        with self.assertInStdOut(["All build-dependencies satisfied"]):
            assert cli.checkbuilddeps(
                "test-source-package-backportable-bpo",
                release=stable_release,
            )

        with self.assertInStdOut(
            ["Unsatisfiable build dependencies", "Build-Depends: no-such-package"]
        ):
            assert not cli.checkbuilddeps(
                "test-source-package-bd-unsatifiable",
                args=["amd64", "sid"],
            )

        with self.assertInStdOut(["No package named"]):
            assert not cli.checkbuilddeps("no-such-package")

    def test_checkbuilddeps_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.checkbuilddeps("no-such-package")

    def test_checkinstall(self, cli: Cli) -> None:
        with self.assertInStdOut(["Depends", "Good: libtesta1, libtestb1"]):
            assert cli.checkinstall("test-package-all-releases")

        with self.assertInStdOut(
            [
                "Good: libtesta1, libtestb1, libtestxa1, libtestxb1, libtestxc1, libtestxd1"
            ]
        ):
            assert cli.checkinstall("test-package-depends-multilevel", args=["sid"])

        with self.assertInStdOut(["No package named"]):
            assert not cli.checkinstall(
                "test-package-stable-backports-only",
            )

        with self.assertInStdOut(["Good: libtesta1, libtestb1"]):
            assert cli.checkinstall(
                "test-package-stable-backports-only",
                args=[f"{stable_release}-backports"],
            )

        with self.assertInStdOut(["No package named"]):
            assert not cli.checkinstall("no-such-package")

        cli.options.verbose = True
        with self.assertInStdOut(["Depends for test-package-all-releases"]):
            assert cli.checkinstall("test-package-all-releases")

    def test_checkinstall_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.checkinstall("no-such-package")

    def test_why(self, cli: Cli) -> None:
        with self.assertInStdOut(["are linked by 1 chain"]):
            assert cli.why("test-package-all-releases", "libtesta1")

        with self.assertInStdOut(["are linked by 1 chain"]):
            assert cli.why(
                "test-package-depends-multilevel", "libtestxd1", release="sid"
            )

        with self.assertInStdOut(["No package named"]):
            assert not cli.why("test-package-depends-multilevel", "libtestxd1")

        with self.assertInStdOut(["No dependency chain could be found"]):
            assert not cli.why("test-package-all-releases", "no-such-package")

        with self.assertInStdOut(["No package named"]):
            assert not cli.why("no-such-package", "test-package-all-releases")

        with self.assertInStdOut(["No package named"]):
            assert not cli.why("no-such-package", "no-such-package")

    def test_why_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.why("no-such-package", "no-such-package")

    def test_checkbackport(self, cli: Cli) -> None:
        """Test the checkbackport command"""
        with self.assertInStdOut(
            [
                "Backport check for test-source-package-backportable-no-bpo",
                "All build-dependencies satisfied",
                "sid",
                stable_release,
            ]
        ):
            assert cli.checkbackport("test-source-package-backportable-no-bpo")

        with self.assertInStdOut(
            [
                "All build-dependencies satisfied",
                f"{stable_release}-backports: libtestxd1",
            ]
        ):
            assert cli.checkbackport("test-source-package-backportable-bpo")

        with self.assertInStdOut(
            ["All build-dependencies satisfied", "virtual: mail-transport-agent"]
        ):
            assert cli.checkbackport("test-source-package-bd-virtual")

        with self.assertInStdOut(
            ["Unsatisfiable build dependencies", "Build-Depends: no-such-package"]
        ):
            assert not cli.checkbackport("test-source-package-bd-unsatifiable")

        with self.assertInStdOut(["No package named"]):
            assert not cli.checkbackport("no-such-package")

    def test_checkbackport_retry(self, cli: Cli, mock_db_failure: MagicMock) -> None:
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            cli.checkbackport("no-such-package")


class TestPackagesCliParseArgs(TestCliBaseParseArg):
    # Note that in testing the main() function below, the logic
    # is inverted as it is a command line utility, returning 0 on success,
    # non-zero on failure

    # pylint: disable=too-many-public-methods

    ##
    ## Test CLI runner with simple options
    ##

    def test_main(self, cli: Cli) -> None:  # type: ignore[override]  # pylint: disable=arguments-renamed
        self._test_general_main(cli)

    def _test(
        self, cli: Cli, udd: Udd, argv: List[str], ret: bool, *args: Any, **kwargs: Any
    ) -> None:
        with self.assertInStdOut(*args, **kwargs):
            r = cli.main(argv, uddInstance=udd)
            assert ret == (not r), kwargs.get("msg")

    def test_invocation_output(self, cli: Cli, udd: Udd) -> None:
        conf = find_test_data_file("test_packages_cli.822")

        def out(test: Deb822, key: str) -> Optional[List[str]]:
            raw = test.get(key, None)
            if raw is None:
                return None
            rawlines = raw.splitlines()
            rawlines = [
                s.lstrip().replace("%stable%", stable_release) for s in rawlines
            ]
            return [s for s in rawlines if s]

        with open(conf, "rt", encoding="UTF-8") as fh:
            for test in Deb822.iter_paragraphs(fh):
                ret = test.get("ret", "True").lower() == "true"
                for cmd in [t for t in test["Cmd"].splitlines() if t]:
                    args = shlex.split(cmd.strip().replace("%stable%", stable_release))
                    self._test(
                        cli,
                        udd,
                        args,
                        ret,
                        out(test, "Out"),
                        out(test, "Not-Out"),
                        out(test, "Err"),
                        out(test, "Not-Err"),
                        msg=f"Generated command: {cmd}",
                    )

    ##
    ## Test options parsing
    ##

    def test_parse_args(  #  type: ignore[override]  # pylint: disable=arguments-renamed
        self, cli: Cli
    ) -> None:
        argv = ["--verbose", "info", "some-package"]
        args = cli.parse_args(argv)
        assert args.verbose

        argv = ["--config", "somefile.conf", "info", "some-package"]
        args = cli.parse_args(argv)
        assert args.config == "somefile.conf"

        argv = ["--version", "info", "some-package"]
        with pytest.raises(SystemExit):
            args = cli.parse_args(argv)

    def _test_package_arch_release_func(self, cli: Cli, cmd: str) -> None:
        argv = [cmd, "some-package"]
        args = cli.parse_args(argv)
        assert args.command == cmd
        assert args.package == "some-package"
        assert args.release is None
        assert args.arch is None
        assert args.details == []

        argv = [cmd, "some-package", "--arch", "some-arch", "--release", "some-release"]
        args = cli.parse_args(argv)
        assert args.command == cmd
        assert args.package == "some-package"
        assert args.release == "some-release"
        assert args.arch == "some-arch"
        assert args.details == []

        argv = [cmd, "some-package", "some-arch", "some-release"]
        args = cli.parse_args(argv)
        assert args.command == cmd
        assert args.package == "some-package"
        assert args.release is None
        assert args.arch is None
        assert args.details == ["some-arch", "some-release"]

    def test_versions(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "versions")

    def test_info(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "info")

    def test_names(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "names")

    def test_archs(self, cli: Cli) -> None:
        argv = ["archs", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "archs"
        assert args.package == "some-package"
        assert args.release is None
        assert args.details == []

        argv = ["archs", "some-package", "--release", "some-release"]
        args = cli.parse_args(argv)
        assert args.command == "archs"
        assert args.package == "some-package"
        assert args.release == "some-release"
        assert args.details == []

        argv = ["archs", "some-package", "some-release"]
        args = cli.parse_args(argv)
        assert args.command == "archs"
        assert args.package == "some-package"
        assert args.release is None
        assert args.details == ["some-release"]

    def test_rprovides(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "rprovides")

        argv = ["whatprovides", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "whatprovides"

    def test_provides(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "provides")

    def test_source(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "source")

    def test_binaries(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "binaries")

    def test_builddeps(self, cli: Cli) -> None:
        for cmd in ["builddeps", "build-deps"]:
            argv = [cmd, "some-package"]
            args = cli.parse_args(argv)
            assert args.command == cmd
            assert args.package == "some-package"
            assert args.release is None
            assert args.details == []

            argv = [cmd, "some-package", "--release", "some-release"]
            args = cli.parse_args(argv)
            assert args.command == cmd
            assert args.package == "some-package"
            assert args.release == "some-release"
            assert args.details == []

            argv = [cmd, "some-package", "some-release"]
            args = cli.parse_args(argv)
            assert args.command == cmd
            assert args.package == "some-package"
            assert args.details == ["some-release"]

    def test_relations(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "relations")

    def test_depends(self, cli: Cli) -> None:
        for rel in [
            "depends",
            "recommends",
            "suggests",
            "enhances",
            "breaks",
            "conflicts",
            "replaces",
        ]:
            self._test_package_arch_release_func(cli, rel)

    def test_recent(self, cli: Cli) -> None:
        argv = ["recent", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "recent"
        assert args.package == "some-package"

    def test_maint(self, cli: Cli) -> None:
        for cmd in ["maint", "maintainer", "changer", "uploader"]:
            argv = [cmd, "some-package"]
            args = cli.parse_args(argv)
            assert args.command == cmd
            assert args.package == "some-package"
            assert args.version is None

            argv = [cmd, "some-package", "--version", "42"]
            args = cli.parse_args(argv)
            assert args.command == cmd
            assert args.package == "some-package"
            assert args.version == "42"

            argv = [cmd, "some-package", "42"]
            args = cli.parse_args(argv)
            assert args.command == cmd
            assert args.package == "some-package"
            assert args.version == "42"

    def test_popcon(self, cli: Cli) -> None:
        argv = ["popcon", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "popcon"
        assert args.package == "some-package"

    def test_checkdeps(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "checkdeps")

        argv = [
            "checkdeps",
            "some-package",
            "--release",
            "some-release",
            "--type",
            "recommends",
        ]
        args = cli.parse_args(argv)
        assert args.deptypes == ["recommends"]

        argv = [
            "checkdeps",
            "some-package",
            "--release",
            "some-release",
            "--type",
            "recommends",
            "--type",
            "suggests",
        ]
        args = cli.parse_args(argv)
        assert args.deptypes == ["recommends", "suggests"]

    def test_checkbuilddeps(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "checkbuilddeps")

    def test_checkinstall(self, cli: Cli) -> None:
        self._test_package_arch_release_func(cli, "checkinstall")

        argv = ["checkinstall", "some-package", "--with-recommends"]
        args = cli.parse_args(argv)
        assert args.withrecommends

    def test_checkbackport(self, cli: Cli) -> None:
        argv = ["checkbackport", "some-package"]
        args = cli.parse_args(argv)
        assert args.command == "checkbackport"
        assert args.package == "some-package"
        assert args.arch is None
        assert args.torelease == "stable"
        assert args.fromrelease == "unstable"

        argv = [
            "checkbackport",
            "some-package",
            "--from-release",
            "dev",
            "--to-release",
            "rel",
            "--arch",
            "some-arch",
        ]
        args = cli.parse_args(argv)
        assert args.command == "checkbackport"
        assert args.package == "some-package"
        assert args.arch == "some-arch"
        assert args.torelease == "rel"
        assert args.fromrelease == "dev"

        argv = [
            "checkbackport",
            "some-package",
            "--arch",
            "some-arch",
            "--fromrelease",
            "dev",
            "--torelease",
            "rel",
        ]
        args = cli.parse_args(argv)
        assert args.command == "checkbackport"
        assert args.package == "some-package"
        assert args.arch == "some-arch"
        assert args.torelease == "rel"
        assert args.fromrelease == "dev"

    def test_why(self, cli: Cli) -> None:
        argv = ["why", "--with-recommends", "pkg1", "pkg2"]
        args = cli.parse_args(argv)
        assert args.command == "why"
        assert args.package1 == "pkg1"
        assert args.package2 == "pkg2"
        assert args.arch is None
        assert args.release is None
        assert args.withrecommends

        argv = [
            "why",
            "--release",
            "some-release",
            "--arch",
            "some-arch",
            "pkg1",
            "pkg2",
        ]
        args = cli.parse_args(argv)
        assert args.command == "why"
        assert args.package1 == "pkg1"
        assert args.package2 == "pkg2"
        assert args.arch == "some-arch"
        assert args.release == "some-release"
        assert not args.withrecommends
