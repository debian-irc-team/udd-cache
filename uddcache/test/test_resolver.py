# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for package resolver """

from typing import Generator, List, Optional, Tuple, Union

import pytest

from uddcache.udd import Udd
from uddcache.relations import (
    RelationshipStatus,
    Relationship,
    BuildDepStatus,
    RelationshipOptions,
)
from uddcache.resolver import Checker, BuildDepsChecker, InstallChecker, SolverHierarchy
from uddcache.packages import PackageNotFoundError, SourcePackage


class TestChecker:
    @pytest.fixture
    def checker(self, udd: Udd) -> Generator[Checker, None, None]:
        yield Checker(udd.BindRelease(arch="amd64", release="sid"))

    def testInit(self) -> None:
        with pytest.raises(TypeError):
            Checker(None)  # type: ignore[arg-type]

    def testCheckRelationsList(self, udd: Udd, checker: Checker) -> None:
        """Test checking the dependencies of a package"""

        # check packages
        checks = [
            # no dependencies
            ("test-no-depends", 0, 0),
            # package where everything is resolvable
            ("test-package-depends-single-level-a", 1, 0),
            # package where everything is resolvable (multiple deps)
            ("test-package-depends-single-level-b", 2, 0),
            # package where everything is resolvable multilevel deps
            ("test-package-depends-multilevel", 2, 0),
            # package with resolvable option groups
            ("test-package-depends-single-level-options", 2, 0),
            # package with unresolvable option groups
            ("test-package-depends-single-level-options-broken-a", 2, 0),
            # package with unresolvable option groups
            ("test-package-depends-single-level-options-broken-b", 2, 0),
            # package with unresolvable deps
            ("test-package-depends-single-unresolvable", 0, 1),
            # package with unresolvable deps
            ("test-package-depends-unresolvable", 1, 1),
            # package with resolvable versioned deps
            ("test-package-versioned-dependencies-resolvable", 1, 0),
            # package with unresolvable versioned deps
            ("test-package-versioned-dependencies-unresolvable", 0, 1),
            # all resolvable, virtual packages
            ("test-package-virtual-dependencies", 1, 0),
        ]

        for pkgname, numgood, numbad in checks:
            p = udd.BindPackage(package=pkgname, release="sid", arch="amd64")
            d = p.RelationshipOptionsList("depends")
            r = checker.CheckRelationshipOptionsList(d)
            assert len(r.good) == numgood
            assert len(r.bad) == numbad
            assert len(r.unchecked) == 0

        # bad input object
        r = checker.CheckRelationshipOptionsList(None)
        assert not r is None

    def testCheckRelationArch(self, checker: Checker) -> None:
        """Check architecture-specific dependency syntax"""
        # NOTE: checker is configured to be sid/amd64
        # nothing specified implies everything satisfies
        assert checker.CheckRelationArch("")  # type: ignore
        assert checker.CheckRelationArch(None)
        assert checker.CheckRelationArch([])
        # single options
        assert not checker.CheckRelationArch(["armhf"])
        assert checker.CheckRelationArch(["amd64"])
        assert not checker.CheckRelationArch(["!amd64"])
        assert checker.CheckRelationArch(["!armhf"])
        # multiple options
        assert checker.CheckRelationArch(["!alpha", "!armhf", "!ia64"])
        assert not checker.CheckRelationArch(["!alpha", "!amd64", "!i386", "!ia64"])
        # wildcard archs
        assert checker.CheckRelationArch(["linux-any"])
        assert checker.CheckRelationArch(["any-amd64"])
        assert not checker.CheckRelationArch(["kfreebsd-any"])
        assert checker.CheckRelationArch(["!kfreebsd-i386"])
        assert not checker.CheckRelationArch(["!linux-any", "!hurd-any"])
        # bad input
        with pytest.raises(TypeError):
            checker.CheckRelationArch(1)  # type: ignore[arg-type]

    def testRelationSatisfied(self, checker: Checker) -> None:
        """Check whether relationships can be satisfied correctly"""
        # NOTE: checker is configured to be sid/amd64

        # Look at each version constraint operator
        # test-package-all-releases has version 1.2.3-1 in the test data
        # >>
        r = Relationship(relation="test-package-all-releases (>> 1.0.1)")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (>> 10:1.0.1)")
        assert not checker.RelationSatisfied(r)
        # <<
        r = Relationship(relation="test-package-all-releases (<< 10.0.1)")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (<< 1.0.1)")
        assert not checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (<< 1.2.3-1)")
        assert not checker.RelationSatisfied(r)
        # =
        r = Relationship(relation="test-package-all-releases (= 1.2.3-1)")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (= 1.2.2-1)")
        assert not checker.RelationSatisfied(r)
        # >=
        r = Relationship(relation="test-package-all-releases (>= 1.2.2-1)")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (>= 1.2.3-1)")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (>= 1.2.4-1)")
        assert not checker.RelationSatisfied(r)
        # <=
        r = Relationship(relation="test-package-all-releases (<= 1.2.4)")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (<= 1.2.3-1)")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="test-package-all-releases (<= 1.2.2-1)")
        assert not checker.RelationSatisfied(r)

        # simple relations
        r = Relationship(relation="test-package-all-releases")
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="no-such-package")
        assert not checker.RelationSatisfied(r)

        # arch specific
        r = Relationship(relation="test-package-all-releases [amd64]")
        assert checker.RelationSatisfied(r)
        r = Relationship(
            relation="test-package-all-releases [armhf]"
        )  # irrelevant for arch
        assert checker.RelationSatisfied(r)
        r = Relationship(
            relation="test-package-sid-armhf-only [armhf]"
        )  # irrelevant for arch
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="no-such-package [armhf]")  # irrelevant for arch
        assert checker.RelationSatisfied(r)
        r = Relationship(relation="no-such-package [amd64]")
        assert not checker.RelationSatisfied(r)

    def testCheck(self, checker: Checker) -> None:
        # list of checks with format (package name, good num, bad num)
        checks = [
            # package where everything is resolvable
            ("test-package-depends-single-level-a", 1, 0),
            # package where everything is resolvable (multiple deps)
            ("test-package-depends-single-level-b", 2, 0),
            # package where everything is resolvable multilevel deps
            ("test-package-depends-multilevel", 2, 0),
            # package with resolvable option groups
            ("test-package-depends-single-level-options", 2, 0),
            # package with unresolvable option groups
            ("test-package-depends-single-level-options-broken-a", 2, 0),
            # package with unresolvable option groups
            ("test-package-depends-single-level-options-broken-b", 2, 0),
            # package with unresolvable deps
            ("test-package-depends-single-unresolvable", 0, 1),
            # package with unresolvable deps
            ("test-package-depends-unresolvable", 1, 1),
            # package with resolvable versioned deps
            ("test-package-versioned-dependencies-resolvable", 1, 0),
            # package with unresolvable versioned deps
            ("test-package-versioned-dependencies-unresolvable", 0, 1),
            # all resolvable, virtual packages
            ("test-package-virtual-dependencies", 1, 0),
        ]

        def deptest(
            b: Optional[RelationshipStatus], good: int, bad: int, msg: str
        ) -> None:
            assert b is not None
            assert b
            assert len(b.good) == good, msg
            assert len(b.bad) == bad, msg

        for pkgname, good, bad in checks:
            b = checker.Check(package=pkgname)
            msg = f"Check for {pkgname}"
            deptest(b, good, bad, msg)

        # non-existent package
        with pytest.raises(PackageNotFoundError):
            checker.Check(package="no-such-package")

        # FIXME: unclear what this is actually testing
        ## conflicts in a package
        # c = checker.Check(package="libc6", relation="conflicts")
        # self.assertTrue(c)
        # self.assertTrue(len(c.good) > 0)
        # self.assertTrue(len(c.bad) == 0)
        ## non-existent conflicts in a package
        # c = checker.Check(package="libapr1", relation="conflicts")
        # self.assertTrue(c)
        # self.assertTrue(len(c.good) > 0)
        # self.assertTrue(len(c.bad) == 0)


class TestBuildDepChecker:
    @pytest.fixture
    def checker(self, udd: Udd) -> Generator[BuildDepsChecker, None, None]:
        yield BuildDepsChecker(udd.BindRelease(arch="amd64", release="sid"))

    def testCheck(self, udd: Udd, checker: BuildDepsChecker) -> None:
        """Test checking the build-dependencies of a package"""

        # checks: package, bd good, bd bad, bdi good, bdi bad
        checks: List[Tuple[Union[str, SourcePackage], int, int, int, int]] = [
            ("test-source-no-bd", 0, 0, 0, 0),
            ("test-source-bdi", 0, 0, 2, 0),
            ("test-package-all-releases", 2, 0, 0, 0),
            ("test-source-package-arch-specific-bd", 3, 0, 0, 0),
            ("test-source-package-bd-unsatifiable", 1, 1, 0, 0),
            ("test-source-package-bdi-unsatifiable", 0, 0, 1, 1),
            ("test-source-package-arch-specific-bd-unsatisfiable", 2, 1, 0, 0),
            # check passing by object
            (
                udd.BindSourcePackage(package="test-package-all-releases"),
                2,
                0,
                0,
                0,
            ),
            # check resolving binary to source package name
            ("test-package-different-name", 1, 0, 0, 0),
        ]

        def bdtest(
            b: BuildDepStatus,
            bdgood: int,
            bdbad: int,
            bdigood: int,
            bdibad: int,
            msg: str,
        ) -> None:
            assert b is not None, msg
            assert b
            assert len(b.bd.good) == bdgood, msg
            assert len(b.bd.bad) == bdbad, msg
            assert len(b.bdi.good) == bdigood, msg
            assert len(b.bdi.bad) == bdibad, msg

        for pkgname, bdgood, bdbad, bdigood, bdibad in checks:
            b = checker.Check(package=pkgname)
            msg = f"BuildDepsCheck for {pkgname}"
            bdtest(b, bdgood, bdbad, bdigood, bdibad, msg)

        # non-existent package
        with pytest.raises(PackageNotFoundError):
            checker.Check(package="no-such-package")

        # test passing bd and bdi lists directly
        # bd list
        p = udd.BindSourcePackage(package="test-package-all-releases")
        bd = p.RelationshipOptionsList("build_depends")
        b = checker.Check(bdList=bd)
        bdtest(b, 2, 0, 0, 0, "direct bdlist test")

        # bdi list
        p = udd.BindSourcePackage(package="test-source-bdi")
        bdi = p.RelationshipOptionsList("build_depends_indep")
        b = checker.Check(bdiList=bdi)
        bdtest(b, 0, 0, 2, 0, "direct bdilist test")

        # both bd and bdi lists
        b = checker.Check(bdList=bd, bdiList=bdi)
        bdtest(b, 2, 0, 2, 0, "direct bdilist+bdilist test")

        # incorrect invocations
        with pytest.raises(ValueError):
            checker.Check()
        with pytest.raises(ValueError):
            checker.Check(bdList=[])  # type: ignore[arg-type]


class TestInstallChecker:
    @pytest.fixture
    def checker(self, udd: Udd) -> Generator[InstallChecker, None, None]:
        yield InstallChecker(udd.BindRelease(arch="amd64", release="sid"))

    def testCheck(self, checker: InstallChecker) -> None:
        """Test installability of packages"""
        # checks: packagename, withrec, result
        checks = [
            ("test-package-all-releases", False, True),
            ("test-package-all-releases", True, True),
            ("test-depends-unsatisfied", False, False),
            ("test-depends-unsatisfied", True, False),
            ("test-recommends", False, True),
            ("test-recommends", True, True),
            ("test-recommends-unsatisfied", False, True),
            ("test-recommends-unsatisfied", True, False),
        ]

        for pkgname, withrec, result in checks:
            c = checker.Check(pkgname, withrec)
            msg = f"Checker {pkgname}"
            assert c is not None, msg
            if result:
                assert "Bad" not in str(c.flatten()), msg
            else:
                assert "Bad" in str(c.flatten()), msg

        with pytest.raises(PackageNotFoundError):
            checker.Check("nosuchpackage")


class TestSolverHierarchy:
    @pytest.fixture
    def checker(self, udd: Udd) -> Generator[InstallChecker, None, None]:
        yield InstallChecker(udd.BindRelease(arch="amd64", release="sid"))

    def testInit(self) -> None:
        s = SolverHierarchy("dpkg")
        assert not s is None

    def testGet(self) -> None:
        s = SolverHierarchy("")
        rs = RelationshipStatus()
        rs.good.append(RelationshipOptions("pkg1"))
        s.depends.extend(rs)
        assert s.get("depends")
        assert not s.get("recommends")
        rs2 = RelationshipStatus()
        rs2.bad.append(RelationshipOptions("pkg2"))
        s.depends.extend(rs2)
        assert not s.get("recommends")

    def testFlatten(self, checker: InstallChecker) -> None:
        s = checker.Check("test-package-depends-multilevel", True)
        assert s is not None
        f = s.flatten()
        assert f
        assert len(f.depends)

    def testChains(self, checker: InstallChecker) -> None:
        s = checker.Check("test-package-depends-multilevel", True)
        assert s is not None
        c = s.chains()
        assert c
        assert len(c)

    def testStr(self, checker: InstallChecker) -> None:
        s = checker.Check("test-package-all-releases", True)
        assert s is not None
        assert str(s)
        f = s.flatten()
        assert str(f)
        s = checker.Check("test-package-depends-multilevel", True)
        assert s is not None
        assert str(s)
        f = s.flatten()
        assert str(f)
