CREATE EXTENSION IF NOT EXISTS debversion WITH SCHEMA public;

-- Based on the UDD setup.sql

-- Sources and Packages
CREATE TABLE sources
(source text, version debversion, maintainer text,
    maintainer_name text, maintainer_email text, format text, files text,
    uploaders text, bin text, architecture text, standards_version text,
    homepage text, build_depends text, build_depends_indep text,
    build_conflicts text, build_conflicts_indep text, priority text, section
    text, distribution text, release text, component text, vcs_type text,
    vcs_url text, vcs_browser text,
    python_version text, ruby_versions text, checksums_sha1 text, checksums_sha256 text,
    original_maintainer text, dm_upload_allowed boolean,
    testsuite text, autobuild text, extra_source_only boolean,
    PRIMARY KEY (source, version, distribution, release, component));

CREATE TABLE packages
(package text, version debversion, architecture text, maintainer text, maintainer_name text, maintainer_email text, description
    text, description_md5 text, source text, source_version debversion, essential text, depends text,
    recommends text, suggests text, enhances text, pre_depends text, breaks text,
    installed_size int, homepage text, size int,
    build_essential text, origin text, sha1 text, replaces text, section text,
    md5sum text, bugs text, priority text, tag text, task text, python_version text,
    ruby_versions text,
    provides text, conflicts text, sha256 text, original_maintainer text,
    distribution text, release text, component text, multi_arch text, package_type text,
PRIMARY KEY (package, version, architecture, distribution, release, component));


-- Bugs (archived and unarchived)

-- udd-cache note: 'bug_severity' type not used here, replace by 'text'
CREATE TABLE bugs
(id int PRIMARY KEY, package text, source text, arrival timestamp, status text,
    severity text, submitter text, submitter_name text,
    submitter_email text, owner text, owner_name text, owner_email text,
    done text, done_name text, done_email text, title text,
    last_modified timestamp, forwarded text, affects_oldstable boolean,
    affects_stable boolean,
    affects_testing boolean, affects_unstable boolean,
    affects_experimental boolean,
    affected_packages text,
    affected_sources text);

CREATE TABLE bugs_packages
(id int REFERENCES bugs, package text, source text,
    PRIMARY KEY (id, package));

CREATE TABLE bugs_merged_with
(id int REFERENCES bugs, merged_with int,
PRIMARY KEY(id, merged_with));

CREATE TABLE bugs_found_in
(id int REFERENCES bugs, version text,
PRIMARY KEY(id, version));

CREATE TABLE bugs_fixed_in
(id int REFERENCES bugs, version text,
PRIMARY KEY(id, version));

CREATE TABLE bugs_tags
(id int REFERENCES bugs, tag text, PRIMARY KEY (id, tag));

CREATE TABLE bugs_blocks
(id int REFERENCES bugs, blocked int,
PRIMARY KEY(id, blocked));

CREATE TABLE bugs_blockedby
(id int REFERENCES bugs, blocker int,
PRIMARY KEY(id, blocker));

-- udd-cache note: 'bug_severity' type not used here, replace by 'text'
CREATE TABLE archived_bugs
(id int PRIMARY KEY, package text, source text, arrival timestamp, status text,
    severity text, submitter text, submitter_name text,
    submitter_email text, owner text, owner_name text, owner_email text,
    done text, done_name text, done_email text, title text,
    last_modified timestamp, forwarded text, affects_oldstable boolean,
    affects_stable boolean,
    affects_testing boolean, affects_unstable boolean,
    affects_experimental boolean,
    affected_packages text,
    affected_sources text);

CREATE TABLE archived_bugs_packages
(id int REFERENCES archived_bugs, package text, source text,
    PRIMARY KEY (id, package));

CREATE TABLE archived_bugs_merged_with
(id int REFERENCES archived_bugs, merged_with int,
PRIMARY KEY(id, merged_with));

CREATE TABLE archived_bugs_found_in
(id int REFERENCES archived_bugs, version text,
PRIMARY KEY(id, version));

CREATE TABLE archived_bugs_fixed_in
(id int REFERENCES archived_bugs, version text,
PRIMARY KEY(id, version));

CREATE TABLE archived_bugs_tags
(id int REFERENCES archived_bugs, tag text, PRIMARY KEY (id, tag));

CREATE TABLE archived_bugs_blocks
(id int REFERENCES archived_bugs, blocked int,
PRIMARY KEY(id, blocked));

CREATE TABLE archived_bugs_blockedby
(id int REFERENCES archived_bugs, blocker int,
PRIMARY KEY(id, blocker));

-- usertags are either for archived or unarchived bugs, so we can't add a
-- foreign key here.
CREATE TABLE bugs_usertags
(email text, tag text, id int);

CREATE TABLE bts_tags (tag text,tag_type text);

-- tags defined in /srv/bugs.debian.org/etc/config on 2015-04-26

INSERT INTO bts_tags (tag,tag_type) VALUES ('patch','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('wontfix','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('moreinfo','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('unreproducible','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('fixed','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('help','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('security','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('upstream','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('pending','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('d-i','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('confirmed','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('ipv6','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('lfs','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('fixed-in-experimental','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('fixed-upstream','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('l10n','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('newcomer','');
-- 'release'-tags are needed by the bugs_rt_affects views
INSERT INTO bts_tags (tag,tag_type) VALUES ('potato','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('woody','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('sarge','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('sarge-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('etch','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('etch-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('lenny','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('lenny-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('squeeze','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('squeeze-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('wheezy','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('wheezy-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('jessie','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('jessie-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('stretch','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('stretch-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('buster','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('buster-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('bullseye','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('bullseye-ignore','');
INSERT INTO bts_tags (tag,tag_type) VALUES ('sid','release');
INSERT INTO bts_tags (tag,tag_type) VALUES ('experimental','release');


-- Popcon

CREATE TABLE popcon (
package text, insts int, vote int, olde int, recent int, nofiles int,
PRIMARY KEY (package));

-- Upload history

CREATE TABLE upload_history
(source text, version debversion, date timestamp with time zone,
changed_by text, changed_by_name text, changed_by_email text, maintainer text, maintainer_name text, maintainer_email text, nmu boolean, signed_by text, signed_by_name text, signed_by_email text, key_id text, distribution text, file text,
fingerprint text,
PRIMARY KEY (source, version));

CREATE TABLE releases (
    release text NOT NULL,
    releasedate date,
    role text,
    releaseversion text,
    distribution text,
    sort integer,
PRIMARY KEY (release)
);

INSERT INTO releases VALUES ( 'etch',                     '2007-04-08', '',             '4.0', 'debian',    400 );
INSERT INTO releases VALUES ( 'etch-security',            '2007-04-08', '',             '4.0', 'debian',    401 ); /* date or NULL ?? */
INSERT INTO releases VALUES ( 'etch-proposed-updates',    '2007-04-08', '',             '4.0', 'debian',    402 ); /* date or NULL ?? */
INSERT INTO releases VALUES ( 'lenny',                    '2009-02-14', '',             '5.0', 'debian',    500 );
INSERT INTO releases VALUES ( 'lenny-security',           '2009-02-14', '',             '5.0', 'debian',    501 ); /* date or NULL ?? */
INSERT INTO releases VALUES ( 'lenny-proposed-updates',   '2009-02-14', '',             '5.0', 'debian',    502 ); /* date or NULL ?? */
INSERT INTO releases VALUES ( 'squeeze',                  '2011-02-06', '',             '6.0', 'debian',    600 );
INSERT INTO releases VALUES ( 'squeeze-security',         '2011-02-06', '',             '6.0', 'debian',    601 );
INSERT INTO releases VALUES ( 'squeeze-proposed-updates', '2011-02-06', '',             '6.0', 'debian',    602 );
INSERT INTO releases VALUES ( 'wheezy',                   '2013-05-05', '', '7.0', 'debian',    700 );
INSERT INTO releases VALUES ( 'wheezy-security',          '2013-05-05', '',             '7.0', 'debian',    701 );
INSERT INTO releases VALUES ( 'wheezy-proposed-updates',  '2013-05-05', '',             '7.0', 'debian',    702 );
INSERT INTO releases VALUES ( 'jessie',                   '2015-04-25', 'oldoldstable',    '8',   'debian',    800 );
INSERT INTO releases VALUES ( 'jessie-security',          '2015-04-25', '',             '8',   'debian',    801 );
INSERT INTO releases VALUES ( 'jessie-proposed-updates',  '2015-04-25', '',             '8',   'debian',    802 );
INSERT INTO releases VALUES ( 'stretch',                  '2017-06-17', 'oldstable',       '9',   'debian',    900 );
INSERT INTO releases VALUES ( 'stretch-security',         '2017-06-17', '',             '9',   'debian',    901 );
INSERT INTO releases VALUES ( 'stretch-proposed-updates', '2017-06-17', '',             '9',   'debian',    902 );
INSERT INTO releases VALUES ( 'buster',                   '2019-07-06', 'stable',      '',    'debian',   1000 );
INSERT INTO releases VALUES ( 'buster-security',          '2019-07-06', '',             '',    'debian',   1001 );
INSERT INTO releases VALUES ( 'buster-proposed-updates',  '2019-07-06', '',             '',    'debian',   1002 );
INSERT INTO releases VALUES ( 'bullseye',                 NULL,         'testing',      '',    'debian',   1100 );
INSERT INTO releases VALUES ( 'bullseye-security',        NULL,         '',             '',    'debian',   1101 );
INSERT INTO releases VALUES ( 'bullseye-proposed-updates', NULL,         '',             '',    'debian',   1102 );
INSERT INTO releases VALUES ( 'sid',                      NULL,         'unstable',     '',    'debian', 100000 );
INSERT INTO releases VALUES ( 'experimental',             NULL,         'experimental', '',    'debian',      0 ); /* this pseudo releases does not fit any order and it is not higher than unstable */


-- screenshots http://screenshots.debian.net/json/screenshots

CREATE TABLE screenshots (
    package text NOT NULL,
    version text,
    homepage text,
    maintainer_name text,
    maintainer_email text,
    description text,
    section text,
    screenshot_url text NOT NULL,
    large_image_url text NOT NULL,
    small_image_url text NOT NULL,
    PRIMARY KEY (small_image_url)
);

