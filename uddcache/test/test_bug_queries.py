# Ultimate Debian Database query tool
#
# Test suite
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Unit tests for generic UDD commands """

from typing import Generator
from unittest.mock import MagicMock

import pytest

from uddcache.udd import Udd, MaxRetriesExceeded
from uddcache.bug_queries import Commands
from uddcache.bts import BugNotFoundError


class TestBugCommands:
    @pytest.fixture
    def dispatcher(self, udd: Udd) -> Generator[Commands, None, None]:
        yield Commands(udd)

    def test_bug(self, dispatcher: Commands) -> None:
        assert dispatcher.bug(600001, False)
        assert dispatcher.bug(600001, True)
        with pytest.raises(BugNotFoundError):
            dispatcher.bug(9999999, True)
        assert dispatcher.bug("600001", True)
        assert dispatcher.bug("#600001", True)

    def test_bug_retry(self, dispatcher: Commands, mock_db_failure: MagicMock) -> None:
        """Test bug lookups w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.bug(9999999, True)

    def test_bug_package(self, dispatcher: Commands) -> None:
        assert not dispatcher.bug_package("test-no-depends")  # no bugs
        assert dispatcher.bug_package("test-package-all-releases")
        assert dispatcher.bug_package("test-package-different-name")
        assert dispatcher.bug_package("src:test-source-package-different-name")
        assert not dispatcher.bug_package("no-such-package")
        assert not dispatcher.bug_package("src:no-such-package")

    def test_bug_package_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test bug lookups w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.bug_package("test-no-depends")

    def test_bug_package_search(self, dispatcher: Commands) -> None:
        assert dispatcher.bug_package_search("test-package-all-releases", "Quite")
        assert not dispatcher.bug_package_search(
            "test-package-all-releases", "Impossible search term"
        )
        assert not dispatcher.bug_package_search("no-such-package", "Some search term")
        assert not dispatcher.bug_package_search(
            "src:no-such-package", "Some search term"
        )

    def test_bug_package_search_retry(
        self, dispatcher: Commands, mock_db_failure: MagicMock
    ) -> None:
        """Test bug lookups w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.bug_package_search("test-no-depends", "boom")

    def test_wnpp(self, dispatcher: Commands) -> None:
        bl = dispatcher.wnpp("test-prospective-package")
        assert len(bl) == 1
        assert bl[0].id == 600010
        bl = dispatcher.wnpp("test-prospective-package", "ITP")
        assert len(bl) == 1
        assert bl[0].id == 600010
        bl = dispatcher.wnpp("test-prospective-package", "RFP")
        assert len(bl) == 0
        bl = dispatcher.wnpp("test-prospective")  # don't match partial package names
        assert len(bl) == 0

    def test_wnpp_retry(self, dispatcher: Commands, mock_db_failure: MagicMock) -> None:
        """Test wnpp lookups w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.wnpp("test-prospective-package")

    def test_rcbugs(self, dispatcher: Commands) -> None:
        bl = dispatcher.rcbugs("test-package-all-releases")
        assert len(bl) == 0  # has bugs but not rc bugs
        bl = dispatcher.rcbugs("test-package-very-buggy")
        assert len(bl) == 3
        bl = dispatcher.rcbugs("test-package-multibin-3")
        assert len(bl) == 1
        bl = dispatcher.rcbugs(
            "test-package-multibin-3"
        )  # test mapping to source package
        assert len(bl) == 1
        bl = dispatcher.rcbugs("no-such-package")
        assert len(bl) == 0

    def test_rc_retry(self, dispatcher: Commands, mock_db_failure: MagicMock) -> None:
        """Test RC bug lookups w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.rcbugs("test-package-all-releases")

    def test_rm(self, dispatcher: Commands) -> None:
        bl = dispatcher.rm("test-package-obsolete")
        assert len(bl) == 1  # there are other RM requests of experimental packages too
        assert bl[0].id == 500030
        bl = dispatcher.rm("no-such-package")
        assert len(bl) == 0

    def test_rm_retry(self, dispatcher: Commands, mock_db_failure: MagicMock) -> None:
        """Test RM lookups w db failure"""
        # pylint: disable=unused-argument
        with pytest.raises(MaxRetriesExceeded):
            dispatcher.rm("test-package-obsolete")
