#
# Ultimate Debian Database query tool
#
# Set piece queries of bug information from the database
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

import re
from typing import Dict, List, Optional, Union

from . import bts
from .udd import Udd, database_retry
from .packages import PackageNotFoundError


class Commands:
    def __init__(self, udd: Udd) -> None:
        self.udd = udd

    @database_retry()
    def bug(self, bugnumber: Union[str, int], verbose: bool) -> bts.Bugreport:
        """
        Retrieve information about a particular bug
        """
        tracker = self.udd.Bts()
        b = tracker.bug(bugnumber)
        if verbose:
            tracker.get_bugs_tags([b])
        return b

    @database_retry()
    def bug_package(
        self,
        package: str,
        verbose: bool = True,
        archived: bool = False,
        source: Optional[bool] = None,
        bugfilter: Optional[Dict[str, Union[str, int]]] = None,
    ) -> List[bts.Bugreport]:
        """
        Retrieve information about bugs in a package
        """
        if bugfilter:
            bugfilter = bugfilter.copy()
        else:
            bugfilter = {}
        if "sort" not in bugfilter:
            bugfilter["sort"] = "id"
        if package[0:4] == "src:":
            packagename = package[4:]
            source = True
        else:
            packagename = package
            if source not in (True, False):
                source = False
        if source:
            try:
                p = self.udd.BindSourcePackage(packagename, self.udd.data.devel_release)
                bugfilter["source"] = p.package
            except PackageNotFoundError:
                bugfilter["source"] = package
        else:
            bugfilter["package"] = package
        tracker = self.udd.Bts(archived)
        bugs = tracker.get_bugs(bugfilter)
        if verbose:
            tracker.get_bugs_tags(bugs)
        return bugs

    def bug_package_search(
        self,
        package: str,
        search: str,
        verbose: bool = True,
        archived: bool = False,
        source: Optional[bool] = None,
    ) -> List[bts.Bugreport]:
        """
        Retrieve information about bugs in a package
        """
        return self.bug_package(package, verbose, archived, source, {"title": search})

    @database_retry()
    def rm(self, package: str, archived: bool = True) -> List[bts.Bugreport]:
        """
        Retrieve information about a package removal bug
        """
        tracker = self.udd.Bts(archived)
        clean_package = re.escape(package)
        return tracker.get_bugs(
            {
                "package": "ftp.debian.org",
                "title": f"RM: {clean_package}",
                "sort": "id",
                "sort_dir": "DESC",
                "limit": 1,
            }
        )

    @database_retry()
    def wnpp(self, package: str, bugtype: Optional[str] = None) -> List[bts.Bugreport]:
        """
        Retrieve information about a package removal bug
        """
        # FIXME there's a wnpp table in the database for this
        tracker = self.udd.Bts(False)
        bugfilter = {
            "package": "wnpp",
            "sort": "id",
            "sort_dir": "DESC",
            "status": list(bts.open_status),
        }
        clean_package = re.escape(package)
        if bugtype:
            bugfilter["title"] = rf"""^["']?{bugtype}\s*(:|--|)\s*{clean_package} """
        else:
            all_types = "|".join(bts.wnpp_types)
            bugfilter["title"] = (
                rf"""^["']?({all_types})\s*(:|--|)\s*{clean_package} """
            )
        return tracker.get_bugs(bugfilter)

    @database_retry()
    def rcbugs(self, package: str, verbose: bool = True) -> List[bts.Bugreport]:
        """
        Retrieve all open release critical bugs for a package
        """
        try:
            p = self.udd.BindSourcePackage(package, self.udd.data.devel_release)
            source = p.package
        except PackageNotFoundError:
            source = package
        tracker = self.udd.Bts(False)  # only consider unarchived bugs
        bugs = tracker.get_bugs(
            {
                "source": source,
                "severity": ["critical", "grave", "serious"],
                "status": ["forwarded", "pending", "pending-fixed"],
            }
        )
        if verbose:
            tracker.get_bugs_tags(bugs)
        return bugs
