#
# Ultimate Debian Database query tool
#
# Configuration for database access
#
###
#
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

r"""
Config file support for udd-cache

Expected format is as follows:
    [database]
    hostname: machine_name
    port:     database_port
    username: database_username
    password: database_password
    database: udd

If working with a Judd instance, this can be generated from
the supybot config file as follows:
    (echo "[database]"; sed -nr 's/.*Judd\.db_(.*)$/\1/p' supybot.conf) \
        > udd-cache.conf

If no configuration is provided, the fallback is to use Debian's public
UDD mirror. Performance won't be great and please don't hammer it!
"""

# See also:
# http://docs.python.org/library/configparser.html

import os
import os.path
from pathlib import Path
import configparser

from typing import Dict, List, Optional, Union

FileLocationType = Union[str, Path]


class Config:
    config: configparser.RawConfigParser

    def __init__(
        self,
        filename: Optional[FileLocationType] = None,
        skipDefaultFiles: bool = False,
        confdict: Optional[Dict[str, str]] = None,
    ) -> None:
        self.filename = filename
        if not confdict:
            self.confdict = {}
        else:
            self.confdict = confdict
        self._ReadConfig(skipDefaultFiles)

    def _ReadConfig(self, skipDefaultFiles: bool = False) -> None:
        """
        Read in the database config for connecting to the UDD instance.

        Returns a dict with these keys:value pairs.
        """
        self.config = configparser.RawConfigParser()
        files: List[str] = []
        if not skipDefaultFiles:
            files.append("/etc/udd-cache.conf")
            files.append(os.path.expanduser("~/.config/udd-cache.conf"))
            files.append("udd-cache.conf")
            files.append(
                os.path.join(os.path.dirname(__file__), "udd-cache-public-mirror.conf")
            )

        if self.filename:
            if not os.path.isfile(self.filename):
                raise IOError(f"Configuration file not found '{self.filename}'.")
            files.append(str(self.filename))

        if "UDD_CACHE_CONFIG" in os.environ and os.environ["UDD_CACHE_CONFIG"]:
            if not os.path.isfile(os.environ["UDD_CACHE_CONFIG"]):
                raise IOError(
                    f"Configuration file not found: '{os.environ['UDD_CACHE_CONFIG']}'."
                )
            files.append(os.environ["UDD_CACHE_CONFIG"])

        files = self.config.read(files)

        if not files and not self.confdict:
            raise ValueError(
                "No valid configuration was found to connect to the database."
            )
        if not self.config.has_section("database"):
            self.config.add_section("database")

        for opt in self.confdict.keys():
            self.config.set("database", opt, self.confdict[opt])

    def db(self) -> Dict[str, Optional[str]]:
        """
        Read in the database config for connecting to the UDD instance.

        Returns a dict with these keys:value pairs.
        """
        conf = {
            "hostname": self.get("database", "hostname", "localhost"),
            "port": self.get("database", "port", "5432"),
            "username": self.get("database", "username", None),
            "password": self.get("database", "password", None),
            "database": self.get("database", "database", "udd"),
        }
        return conf

    def get(
        self, section: str, value: str, default: Optional[str] = None
    ) -> Optional[str]:
        try:
            return self.config.get(section, value)
        except (KeyError, configparser.NoSectionError, configparser.NoOptionError):
            return default
