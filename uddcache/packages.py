###
# -*- coding: utf-8 -*-
#
# Copyright (c) 2007,2008, Mike O'Connor
# Copyright (c) 2010-2011  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES LOSS OF USE, DATA, OR PROFITS OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import re

from typing import Any, Dict, List, Optional, Tuple, Union

from psycopg import sql
from psycopg.rows import dict_row

from .relations import RelationshipOptions, RelationshipOptionsList
from . import data

from .database import UddDatabase, database_retry


ReleaseSpecificationType = Optional[Union[str, List[str], Tuple[str, ...]]]


class Release:
    """Class that represents the contents of a release
    i.e lists of binary and source packages"""

    release: Tuple[str, ...]

    def __init__(
        self,
        udd: UddDatabase,
        arch: Optional[str] = None,
        release: ReleaseSpecificationType = None,
        pins: Optional[Dict[str, int]] = None,
    ) -> None:
        self.udd = udd
        self.arch = arch
        if self.arch is None:
            self.arch = data.DebianData.default_arch
        if release is None:
            release = data.DebianData.stable_release
        if isinstance(release, (tuple, list)):
            self.release = tuple(release)
        else:
            self.release = (release,)
        self.pins = pins
        self.cache: Dict[str, Package] = {}
        self.scache: Dict[str, SourcePackage] = {}

    def Package(
        self,
        package: Union[str, "Package"],
        version: Optional[str] = None,
        operator: Optional[str] = None,
    ) -> "Package":
        """Look up a binary package by name in the current release.
        package: name of the package (string)
        returns Package object
        """
        if isinstance(package, Package):
            package = package.package

        phash = self._mkpackagehash(package, operator, version)
        if not phash in self.cache:
            self.cache[phash] = Package(
                self.udd,
                arch=self.arch,
                release=self.release,
                package=package,
                pins=self.pins,
                version=version,
                operator=operator,
            )
        return self.cache[phash]

    def Source(
        self,
        package: Union[str, "SourcePackage"],
        autoBin2Src: bool = True,
        version: Optional[str] = None,
        operator: Optional[str] = None,
    ) -> "SourcePackage":
        """Look up a source package by name in the current release.
        package: name of the package (string)
        autoBin2Src: (default true) convert names of binary packages to
                        source packages automatically if needed
        returns Package object
        """
        if isinstance(package, SourcePackage):
            package = package.package

        phash = self._mkpackagehash(package, operator, version)
        if not phash in self.scache:
            self.scache[phash] = SourcePackage(
                self.udd,
                arch=self.arch,
                release=self.release,
                package=package,
                pins=self.pins,
                version=version,
                operator=operator,
            )
            if autoBin2Src and not self.scache[phash].Found():
                p = self.bin2src(package)
                return self.Source(p, False)
        if not self.scache[phash].Found():
            raise PackageNotFoundError(package)
        return self.scache[phash]

    @database_retry()
    def bin2src(self, package: Union[str, "AbstractPackage"]) -> str:
        """Returns the source package for a given binary package"""
        with self.udd.cursor() as c:
            c.execute(
                r"""SELECT source
                    FROM packages
                    WHERE package=%(package)s
                    AND release = ANY(%(release)s)
                    LIMIT 1""",
                {"package": str(package), "release": list(self.release)},
            )
            row = c.fetchone()
            if row:
                return row[0]  # type: ignore

            raise PackageNotFoundError(package)

    def arch_applies(self, proposed: str) -> bool:
        def kern_arch_split(archspec: str) -> Tuple[str, str]:
            if "-" in archspec:
                return archspec.split("-", maxsplit=2)  # type: ignore
            return ("linux", archspec)

        if proposed in (self.arch, "all"):
            return True

        (pkern, parch) = kern_arch_split(proposed)
        (skern, sarch) = (
            kern_arch_split(self.arch) if self.arch is not None else ("", "")
        )

        if pkern == "any":
            return sarch == parch
        if parch == "any":
            return skern == pkern
        return parch == sarch and pkern == skern

    @staticmethod
    def _mkpackagehash(
        package: str, operator: Optional[str], version: Optional[str]
    ) -> str:
        return f"{package}|{operator}|{version}"

    def __str__(self) -> str:
        return (
            f"Release: {self.release}.\n"
            f"\t{len(self.cache)} binary packages and {len(self.scache)} source packages in cache."
        )


class AbstractPackage:
    fields = ["*"]
    table = ""
    column = ""
    pins: Optional[Dict[str, int]]
    data: Optional[Dict[str, Any]]
    release: Tuple[str, ...]
    package: str

    def __init__(
        self,
        udd: UddDatabase,
        arch: Optional[str] = None,
        release: ReleaseSpecificationType = None,
        package: Optional[Union[str, "AbstractPackage"]] = None,
        pins: Optional[Dict[str, int]] = None,
        version: Optional[str] = None,
        operator: Optional[str] = None,
    ) -> None:
        """
        Bind a specified binary or source package.

        A single release or a list or tuple of releases can be used. If more
        than one release is given, a set of pins giving the priority of the
        different releases in searching for packages can be specified. The pins
        should be a dict of releasename => pin value where higher values are
        preferred. In the absence of any specified pins, all releases are
        treated equally, meaning that the most recent package version will be
        chosen.

        Note that the "pinning" used here is a very simple rank order not the
        sophisticated system used by apt as described in apt_preferences(5).
        """
        if not package:
            raise ValueError("Package name not specified")
        #        if type(package) is Package:
        #            return package
        if isinstance(package, AbstractPackage):
            package = package.package
        elif not isinstance(package, str):
            raise ValueError(f"What did you do to 'package'? It was a {type(package)}")
        self.udd = udd
        self.arch = arch
        if self.arch is None:
            self.arch = data.DebianData.default_arch
        if release is None:
            release = data.DebianData.stable_release
        if isinstance(release, tuple):
            self.release = release
        elif isinstance(release, list):
            self.release = tuple(release)
        elif isinstance(release, str):
            self.release = (release,)
        else:
            raise ValueError(
                f"Release needs to be a str, tuple, list. It was a {type(release)}"
            )
        self.package = package
        if pins and not isinstance(pins, dict):
            raise ValueError(
                "List of pins must be a dict mapping the "
                "release name to its relative importance."
            )
        self.pins = pins
        self.version = version
        self.operator = operator
        self._Fetch()

    @database_retry()
    def _Fetch(self) -> None:
        with self.udd.cursor(row_factory=dict_row) as c:
            sql_fields: sql.Composable
            if self.fields[0] == "*" and len(self.fields) == 1:
                sql_fields = sql.SQL("*")
            else:
                sql_fields = sql.SQL(",").join([sql.Identifier(f) for f in self.fields])
            pin = sql.SQL("")
            pin_cases: sql.Composable = sql.SQL("")
            if self.pins and self.release:
                cases = [
                    sql.SQL("WHEN release = {release} THEN {pin}").format(
                        release=sql.Literal(release),
                        pin=sql.Literal(pin_value),
                    )
                    for release, pin_value in self.pins.items()
                ]
                pin_cases = (
                    sql.SQL(", CASE ")
                    + sql.SQL(" ").join(cases)
                    + sql.SQL(" ELSE 0 END AS pin ")
                )
                pin = sql.SQL("pin DESC, ")
            version_where: sql.Composable = sql.SQL("")
            if self.version and self.operator:
                version_where = sql.SQL(
                    "AND version {op} debversion({version})"
                ).format(
                    op=sql.SQL(self.operator.replace(">>", ">").replace("<<", "<")),
                    version=sql.Literal(self.version),
                )
            query = sql.SQL(
                r"""SELECT
                            {fields}
                            {pin_cases}
                        FROM
                            {table}
                        WHERE
                            {column} = %(package)s
                            AND (architecture='all' OR architecture=%(arch)s)
                            AND release = ANY(%(release)s)
                            {version_where}
                        ORDER BY
                            {pin} version DESC
                        LIMIT 1""",
            ).format(
                fields=sql_fields,
                pin_cases=pin_cases,
                table=sql.Identifier(self.table),
                column=sql.Identifier(self.column),
                version_where=version_where,
                pin=pin,
            )
            # logger.debug("AbstractPackage._Fetch:%s", query)
            c.execute(
                query,
                {
                    "package": self.package,
                    "arch": self.arch,
                    "release": list(self.release),
                },
            )
            self.data = c.fetchone()

    def Found(self) -> bool:
        """Does the package exist in the database for the release specified?

        For binary packages this is equivalent to:
            "Is the package a real package?"
        (returns false for packages that are only virtual packages)"""
        return self.data is not None

    def RelationEntry(self, relation: str, combinePreDepends: bool = True) -> str:
        if not self.Found():
            raise PackageNotFoundError(self.package)
        assert self.data is not None  # just to keep mypy happy
        if relation == "depends" and combinePreDepends:
            rs = [self.data["depends"], self.data["pre_depends"]]
            return ", ".join(filter(None, rs))
        return self.data[relation]  # type: ignore

    def RelationshipOptionsList(
        self, relation: str, combinePreDepends: bool = True
    ) -> RelationshipOptionsList:
        rels = RelationshipOptionsList()
        l = self.RelationEntry(relation, combinePreDepends)
        if l:
            for r in re.split(r"\s*,\s*", l):
                roptions = RelationshipOptions(r)
                rels.append(roptions)
        return rels


class Package(AbstractPackage):
    def __init__(
        self,
        udd: UddDatabase,
        arch: Optional[str] = None,
        release: ReleaseSpecificationType = None,
        package: Optional[Union[str, "AbstractPackage"]] = None,
        pins: Optional[Dict[str, int]] = None,
        version: Optional[str] = None,
        operator: Optional[str] = None,
    ) -> None:
        """
        Bind a specified binary package from a releases, list of releases
        or tuple of releases

        """
        self.table = "packages"
        self.column = "package"
        self._ProvidersList: Optional[List[Tuple[str, Optional[str]]]] = None
        self.installable = None
        AbstractPackage.__init__(
            self,
            udd,
            arch,
            release,
            package,
            pins=pins,
            version=version,
            operator=operator,
        )

    def IsVirtual(self, version: Optional[str] = None) -> bool:
        """Test if the package is a virtual package.

        Tests to see if any package Provides the current package
        """
        return len(self.ProvidersList(version)) > 0

    def IsVirtualOnly(self) -> bool:
        """Test if the package is a (purely) virtual package."""
        return not self.Found() and self.IsVirtual()

    def IsAvailable(self) -> bool:
        return self.Found() or self.IsVirtual()

    @database_retry()
    def ProvidersList(
        self, version: Optional[str] = None
    ) -> List[Tuple[str, Optional[str]]]:
        if self._ProvidersList is None:
            # remove all characters from the package name that aren't legal in
            # a package name i.e. not in:
            #    a-z0-9-.+
            # see §5.6.1 of Debian Policy "Source" for details.
            # http://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-Source
            #
            # \m is start word boundary, \M is finish word boundary
            # (but - in package name is a word boundary)
            # \A is start string,        \Z is finish string
            # http://www.postgresql.org/docs/8.3/static/functions-matching.html
            packagere = re.escape(re.sub(r"[^a-z\d+-]", "", self.package))
            fieldre = rf"(?:\A|,\s*){packagere}(?:\s+\(\s*=\s*(\d[\d:\w.+~-]*)\s*\))?(?=(?:\Z|\s*,))"
            fieldlike = r"%%%s%%" % re.sub(r"([%_])", r"\\1", self.package)
            with self.udd.cursor() as c:
                c.execute(
                    r"""SELECT DISTINCT package, provides
                        FROM packages
                        WHERE
                            provides LIKE %(fieldlike)s
                            AND provides ~ %(fieldre)s
                            AND (architecture='all' OR architecture=%(arch)s)
                            AND release = ANY(%(release)s)""",
                    {
                        "fieldlike": fieldlike,
                        "fieldre": fieldre,
                        "arch": self.arch,
                        "release": list(self.release),
                    },
                )
                pkgs = []
                for row in c.fetchall():
                    for match in re.finditer(fieldre, row[1]):
                        ver = match.group(1)
                        pkgs.append((row[0], ver))
                self._ProvidersList = pkgs

        # now filter the list if there is a version constraint
        if version:
            providers = [p for p in self._ProvidersList if p[1] == version]
        else:
            providers = self._ProvidersList
        return providers

    def PreDepends(self) -> str:
        return self.RelationEntry("pre_depends")

    def PreDependsList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("pre_depends")

    def Depends(self, combinePreDepends: bool = True) -> str:
        return self.RelationEntry("depends", combinePreDepends)

    def DependsList(self, combinePreDepends: bool = True) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("depends", combinePreDepends)

    def Recommends(self) -> str:
        return self.RelationEntry("recommends")

    def RecommendsList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("recommends")

    def Suggests(self) -> str:
        return self.RelationEntry("suggests")

    def SuggestsList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("suggests")

    def Enhances(self) -> str:
        return self.RelationEntry("enhances")

    def EnhancesList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("enhances")

    def Conflicts(self) -> str:
        return self.RelationEntry("conflicts")

    def ConflictsList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("conflicts")

    def Breaks(self) -> str:
        return self.RelationEntry("breaks")

    def BreaksList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("breaks")

    def Replaces(self) -> str:
        return self.RelationEntry("replaces")

    def ReplacesList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("replaces")

    def __str__(self) -> str:
        return f"Package {self.package} on {self.arch} in release {self.release}"


class SourcePackage(AbstractPackage):
    def __init__(
        self,
        udd: UddDatabase,
        arch: Optional[str] = None,
        release: ReleaseSpecificationType = None,
        package: Optional[Union[str, "SourcePackage"]] = None,
        pins: Optional[Dict[str, int]] = None,
        version: Optional[str] = None,
        operator: Optional[str] = None,
    ) -> None:
        # self.fields = ['build_depends', 'build_depends_indep', 'version']
        self.table = "sources"
        self.column = "source"
        # self.autobin2src = kwargs.get('bin2src', True)
        AbstractPackage.__init__(
            self,
            udd,
            arch,
            release,
            package,
            pins=pins,
            version=version,
            operator=operator,
        )

    @database_retry()
    def _Fetch(self) -> None:
        with self.udd.cursor(row_factory=dict_row) as c:
            f = ",".join(self.fields)
            c.execute(
                r"""SELECT """
                + f
                + """
                    FROM """
                + self.table
                + """
                    WHERE """
                + self.column
                + """=%(package)s
                        AND release = ANY(%(release)s)
                    ORDER BY version DESC
                    LIMIT 1""",
                {
                    "package": self.package,
                    "release": list(self.release),
                },
            )
            self.data = c.fetchone()

    @database_retry()
    def Binaries(self) -> List[str]:
        with self.udd.cursor() as c:
            c.execute(
                r"""SELECT DISTINCT package
                    FROM packages
                    WHERE source=%(package)s
                        AND release = ANY(%(release)s)""",
                {
                    "package": self.package,
                    "arch": self.arch,
                    "release": list(self.release),
                },
            )
            pkgs = []
            for row in c.fetchall():
                pkgs.append(row[0])
            return pkgs

    def BuildDepends(self) -> str:
        return self.RelationEntry("build_depends")

    def BuildDependsList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("build_depends")

    def BuildDependsIndep(self) -> str:
        return self.RelationEntry("build_depends_indep")

    def BuildDependsIndepList(self) -> RelationshipOptionsList:
        return self.RelationshipOptionsList("build_depends_indep")

    def __str__(self) -> str:
        return f"Source {self.package} in release {self.release}"


class PackageNotFoundError(LookupError):
    """Exception raised when a package is assumed to exist but doesn't"""

    def __init__(self, package: Union[str, AbstractPackage]) -> None:
        super().__init__()
        self.package = package

    def __str__(self) -> str:
        return f"Package was not found: {self.package}"
