# coding: utf-8

#
# Ultimate Debian Database query tool
#
# CLI bindings
#
###
# Copyright (c) 2010-2021  Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
###

""" Command line interface to udd - output to stdout """

import argparse
from pathlib import Path

from typing import Any, Dict, List, Optional, Union

from . import clibase
from . import udd
from . import bug_queries
from . import bts
from . import __version__
from .config import Config


class Cli(clibase.CliBase):
    """Run a specified command sending output to stdout"""

    def __init__(
        self,
        config: Optional[Union[str, Path, Config]] = None,
        options: Any = None,
        initialise: bool = True,
        uddInstance: Optional[udd.Udd] = None,
    ) -> None:
        super().__init__(config, options, initialise, bug_queries.Commands, uddInstance)

    def bug(self, search: str, keywords: Optional[str] = None) -> bool:
        if search.isdigit() and int(search) < 1e7:
            return self._bug_by_number(int(search))

        if search.startswith("#") and search.replace("#", "").isdigit():
            return self.bug(search.replace("#", ""), keywords)

        if not keywords:
            return self._bug_by_package(search)

        return self._bug_by_package_keywords(search, keywords)

    def _bug_by_number(self, bugnumber: int) -> bool:
        # bug number search
        try:
            bug = self.dispatcher.bug(bugnumber, self.options.verbose)
        except bts.BugNotFoundError:
            print(f"Sorry, bug {bugnumber} was not found.")
            return False
        print(bug)
        return True

    def _bug_by_package(self, package: str) -> bool:
        # package name search
        bugs = self.dispatcher.bug_package(
            package,
            verbose=self.options.verbose,
            archived=False,
            bugfilter={"status": ["forwarded", "pending", "pending-fixed"]},
        )
        found = bool(bugs)
        if self.options.verbose:
            print("\n".join([str(b) for b in bugs]))
        else:
            for s in bts.severities:
                bs = [str(b.id) for b in bugs if b.severity == s]
                if bs:
                    print(f"{s}: {len(bs)}: {', '.join(bs)}")
        bugs = self.dispatcher.wnpp(package)
        found |= bool(bugs)
        for s in bts.wnpp_types:
            bl = [b for b in bugs if b.wnpp_type == s]
            if bl:
                print(f"{s}: #{bl[0].id}")
        bugs = self.dispatcher.rm(package)
        found |= bool(bugs)
        if bugs:
            print("RM:", ",".join(f"#{b.id}" for b in bugs))
        if not found:
            print(f"Sorry, no bugs for '{package}' were found.")
        return found

    def _bug_by_package_keywords(self, package: str, keywords: str) -> bool:
        # keyword search
        bugs = self.dispatcher.bug_package_search(
            package, keywords, verbose=self.options.verbose, archived=False
        )
        if self.options.verbose:
            print("\n".join([str(b) for b in bugs]))
        else:
            allbugs = [f"#{b.id}: {b.title}" for b in bugs]
            print("\n".join(allbugs))
        return bool(bugs)

    def rm(self, package: str) -> bool:
        bugs = self.dispatcher.rm(package)
        if not bugs:
            print(f"Sorry, no removal bug for {package} was found.")
            return False
        print(bugs[0])
        return True

    def wnpp(self, package: str, command: str = "wnpp") -> bool:
        # pylint: disable=unused-argument
        bugtype: Optional[str] = command.upper()
        if bugtype == "ORPHAN":
            bugtype = "O"
        if bugtype == "WNPP":
            bugtype = None
        bugs = self.dispatcher.wnpp(package, bugtype)
        if not bugs:
            bugtypestr = bugtype if bugtype else "WNPP"
            print(f"Sorry, no {bugtypestr} bug for {package} was found.")
            return False
        print("\n".join([str(b) for b in bugs]))
        return True

    def rfs(self, package: str) -> bool:
        bugfilter: Dict[str, Any] = {"title": package}
        if not self.options.verbose:  # also get fixed bugs for verbose
            bugfilter["status"] = ["forwarded", "pending", "pending-fixed"]
        bugs = self.dispatcher.bug_package(
            "sponsorship-requests",
            verbose=True,  # always get tags
            archived=self.options.verbose,
            bugfilter=bugfilter,
        )
        if not bugs:
            print("No open RFS bugs found for that package")
            return False
        for b in bugs:
            s = [
                f"Bug: {b.id}",
                f"Title: {b.title.splitlines()[0]}",
                f"Severity: {b.severity}",
                f"Status: {b.readable_status}",
                f"Opened: {b.arrival.date()}",
                f"Last-Modified: {b.last_modified.date()}",
                f"Tags: {', '.join(b.tags)}",
                f"Submitter: {b.submitter}",
                f"Owner: {b.owner}",
                "",
            ]
            print("\n".join(s))
        return True

    def rcbugs(self, package: str) -> bool:
        bugs = self.dispatcher.rcbugs(package)
        if not bugs:
            print(f"No release critical bugs were found for '{package}'.")
            return False
        print("\n".join([str(b) for b in bugs]))
        return True

    @staticmethod
    def parse_args(argv: Optional[List[str]] = None) -> argparse.Namespace:
        description = """\
%(prog)s queries an Ultimate Debian Database instance to make it possible
to perform simple bug-related queries."""

        parser = argparse.ArgumentParser(description=description, epilog="")

        # create sub commands for each import action to be performed
        commands = parser.add_subparsers(dest="command", required=True)

        # subcommands

        ### "bug"
        subparser = commands.add_parser("bug", help="display information about a bug.")
        subparser.add_argument(
            "search",
            action="store",
            metavar="BUG|PACKAGE",
            help="bug number or package to display information about",
        )
        subparser.add_argument(
            "keywords",
            action="store",
            metavar="keywords",
            nargs="?",
            help="keywords to look for in bug titles",
        )
        subparser.set_defaults(func=lambda r, a: r.bug(a.search, a.keywords))

        ### "rm"
        subparser = commands.add_parser(
            "rm",
            help="display information about a package removal bug.",
        )
        subparser.add_argument(
            "package",
            action="store",
            metavar="PACKAGE",
            help="package to search for removal bugs.",
        )
        subparser.set_defaults(func=lambda r, a: r.rm(a.package))

        ### "wnpp" (including all types)
        subparser = commands.add_parser(
            "wnpp",
            aliases=["rfp", "itp", "rfh", "rfa", "ita", "orphan", "o"],
            help="display information about a WNPP bug for a package. "
            "rfp, itp, rfh, rfa, ita, and orphan can be used to search for "
            "specific types of WNPP bug.",
        )
        # FIXME how can this be made case insensitive?
        subparser.add_argument(
            "package",
            action="store",
            metavar="PACKAGE",
            help="package to search for WNPP information.",
        )
        subparser.set_defaults(func=lambda r, a: r.wnpp(a.package, command=a.command))

        ### "rfs"
        subparser = commands.add_parser(
            "rfs",
            help="display request for sponsorship bug information",
        )
        subparser.add_argument(
            "package",
            action="store",
            metavar="PACKAGE",
            help="package to search for RFS information.",
        )
        subparser.set_defaults(func=lambda r, a: r.rfs(a.package))

        ### "rcbugs"
        subparser = commands.add_parser(
            "rcbugs",
            help="display RC bug information",
        )
        subparser.add_argument(
            "package",
            action="store",
            metavar="PACKAGE",
            help="package to search for RC bug information.",
        )
        subparser.set_defaults(func=lambda r, a: r.rcbugs(a.package))

        ## General options
        # No support for other distributions just yet, but in time...
        # parser.add_argument(
        # "--distro", dest='distro', default='debian',
        # help="the distribution to be queried in searches "
        # "(examples: --distro=debian --distro=ubuntu)"
        # )
        parser.add_argument(
            "--config",
            dest="config",
            metavar="FILE",
            default=None,
            help="the UDD database connection configuration file",
        )
        parser.add_argument(
            "-v",
            "--verbose",
            action="count",
            dest="verbose",
            default=0,
            help="include additional information in output",
        )
        parser.add_argument(
            "--version",
            action="version",
            version="%(prog)s " + str(__version__),
            help="print version string and exit",
        )

        args = parser.parse_args(argv)

        # No support for other distributions just yet, but in time...
        args.distro = "debian"
        return args


main = Cli.main
